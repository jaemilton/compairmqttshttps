1 - Configuring mosquitto server
    1.1 - Give script execution permission --> chmod u+x server/prepare_mosquitto_server.sh
    1.2 - run the script server/prepare_mosquitto_server.sh

2 - Configure WebSocket Flask Server
    2.1 - Give script execution permission --> chmod u+x server/http/prepare_https_flask_server.sh
    2.2 - run the script server/http/prepare_https_flask_server.sh

3 - Configure mqtt client
    3.1 - Give script execution permission --> chmod u+x client/prepare_mqtt_client.sh
    3.2 - run the script client/prepare_mqtt_client.sh

4 - Configure WebSocket client
    4.1 - Give script execution permission --> chmod u+x client/prepare_websocket_client.sh
    4.2 - run the script client/prepare_websocket_client.sh