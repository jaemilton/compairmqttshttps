#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
import sys
import threading


import socketio
from engineio.payload import Payload

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'common_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from common_lib.common_base import CommonBase
from common_lib.client_type import ClientType
from common_lib.websocket_client_type import WebsocketClientType
from websocket_handler import WebsocketHandler
from sockeio_handler import SocketioHandler

class HttpsWebSocketClientBase(CommonBase):
    # Constructor
    def __init__(self,
                 url_websocket_server: str,
                 event_to_publish: str,
                 event_to_listening: str,
                 ca_cert_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 on_message_callback,
                 on_start_handler,
                 on_stop_handler=None,
                 websocket_client_type: WebsocketClientType = WebsocketClientType.WEBSOCKET,
                 debug: bool = False):
        CommonBase.__init__(self, on_start_handler=on_start_handler, on_stop_handler=on_stop_handler, debug=debug)
        self._websocket_handler = None

        if websocket_client_type == WebsocketClientType.SOCKEIO:
            self._websocket_handler = SocketioHandler(event_to_publish = event_to_publish,
                                                        url_websocket_server = url_websocket_server,
                                                        ca_cert_path = ca_cert_path,
                                                        certfile_path=certfile_path,
                                                        keyfile_path=keyfile_path,
                                                        event_to_listening=  event_to_listening,
                                                        on_message_callback = on_message_callback,
                                                        running_function_callback = self.is_running, 
                                                        debug = debug)
        else:
            self._websocket_handler = WebsocketHandler(event_to_publish = event_to_publish,
                                                        url_websocket_server = url_websocket_server,
                                                        ca_cert_path = ca_cert_path,
                                                        certfile_path=certfile_path,
                                                        keyfile_path=keyfile_path,
                                                        event_to_listening=  event_to_listening,
                                                        on_message_callback = on_message_callback,
                                                        running_function_callback = self.is_running, 
                                                        debug = debug)
        
    def event_to_listening(self):
        return self._websocket_handler.event_to_listening

    # uncomment for line_profiler
    # @profile
    def _publish_message(self, message_id, message):
        self._websocket_handler.publish_message(message_id=message_id, message=message)

    def _connect(self, client_type: ClientType = ClientType.BOTH):
        if client_type == ClientType.BOTH:
            self._websocket_handler.connect_subscriber()
            self._websocket_handler.connect_publisher()
        elif client_type == ClientType.PUBLISHER:
            self._websocket_handler.connect_publisher()
        else:
            self._websocket_handler.connect_subscriber()

    def _disconnect(self, client_type: ClientType = ClientType.BOTH):
        if client_type == ClientType.BOTH:
            self._websocket_handler.disconnect_publisher()
            self._websocket_handler.disconnect_subscriber()
        elif client_type == ClientType.PUBLISHER:
            self._websocket_handler.disconnect_publisher()
        else:
            self._websocket_handler.disconnect_subscriber()
