#!/usr/bin/python3import
# -*- coding: UTF-8 -*-
import os
import sys
import datetime



# including some directories to PATH 
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'message_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from common_lib.common_base import valid_mandatory_parameters
from common_lib.common_base import get_input_parameter_value
from common_lib.websocket_client_type import WebsocketClientType
from common_lib.client_type import ClientType
from client.publisher_base import PublisherBase
from client.https_websocket_client_base import HttpsWebSocketClientBase
from common_lib.common_error import BadUserInputError


class HttpsWebSocketClientPublisher(HttpsWebSocketClientBase, PublisherBase):

    # Constructor
    def __init__(self,
                 url_websocket_server: str,
                 client_name: str,
                 event_to_publish: str,
                 event_to_listening: str,
                 message: str,
                 ca_cert_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 message_count: int = 1,
                 log_directory: str = None,
                 websocket_client_type: WebsocketClientType = WebsocketClientType.WEBSOCKET,
                 debug: bool = False):
        HttpsWebSocketClientBase.__init__(self=self,
                                          url_websocket_server=url_websocket_server,
                                          event_to_publish=event_to_publish,
                                          event_to_listening=event_to_listening,
                                          ca_cert_path=ca_cert_path,
                                          certfile_path=certfile_path,
                                          keyfile_path=keyfile_path,
                                          on_message_callback=self.on_message,
                                          on_start_handler=self.on_start,
                                          on_stop_handler=self.on_stop,
                                          websocket_client_type= websocket_client_type,
                                          debug=debug)
        if not log_directory:
            log_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "logs")

        csv_file_name = "{0}_{1}.csv".format(type(self).__name__,
                                                    datetime.datetime.now().strftime(
                                                        "%Y%m%d%H%M%S"), 'w', newline='')

        PublisherBase.__init__(self=self,
                               message_count=message_count,
                               log_file_name=csv_file_name,
                               log_directory=log_directory,
                               client_name=client_name,
                               publisher_method_callback=self._publish_message,
                               message=message,
                               on_publish_finished=self._on_publish_finished,
                               debug=debug)
        

    def on_start(self):
        self._connect()
        self._on_start()


    def _on_publish_finished(self):
        self._disconnect(client_type=ClientType.PUBLISHER)

    def on_stop(self):
        self._disconnect()
        self._on_stop()

   

    def on_message(self, message):
        self._on_message(self.event_to_listening(), message)
        if self.all_response_messages_received():
            print("All " + str(self._message_count) + " sent were responded.")
            self.stop()


def start_websocket_client(publisher_name,
                           url_websocket_server,
                           event_to_publish,
                           event_to_listening,
                           message,
                           message_count,
                           log_directory,
                           websocket_client_type,
                           debug):
    ca_cert_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        'certs',
        'mqtt-ca.crt')

    certfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-publisher.crt")

    keyfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-publisher.key")

    mqtt_client_publisher = HttpsWebSocketClientPublisher(url_websocket_server=url_websocket_server,
                                                          client_name=publisher_name,
                                                          event_to_publish=event_to_publish,
                                                          event_to_listening=event_to_listening,
                                                          message=message,
                                                          ca_cert_path=ca_cert_path,
                                                          certfile_path=certfile_path,
                                                          keyfile_path=keyfile_path,
                                                          message_count=message_count,
                                                          log_directory=log_directory,
                                                          websocket_client_type=websocket_client_type,
                                                          debug=debug)

    mqtt_client_publisher.start()


def start(argv):
    if '-?' in argv:
        print("""Flask SocketIO Client to send a blast of messages to stress test Usage: python3 
        client_https_websocket.py -n publisher_name -a url_websocket_server -e event_name_to_publish -m message 
        [-s topic_to_subscribe] [-c message_count] [-l log_directory] [-t websocket_client_type] [-?] [-v] 

                -n : name of publisher, that will be sent inside the messages.
                -a : url address of https websocket server
                -e : event name to send the messages
                -m : message that will be sent 
                -s : event name to receive messages from websocket server, this is optional.
                -c : number of messages to me sent on stress test
                -l : Optional, log directory, if not defined, will create a folter logs on current directory
                -t : websocket connection type, values are SOCKEIO or WEBSOCKET, if not set, will assume WEBSOCKET
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    elif valid_mandatory_parameters(argv, ['-n', '-a', '-e', '-m']):
        raise BadUserInputError(
            "Input error. To run, call as python3 client_https_websocket.py -n publisher_name -e topic_to_publish "
            "-m message [-s topic_to_subscribe] [-c message_count] [-l log_directory] [-t websocket_client_type] [-?] [-v]")
    else:
        websocket_client_type_param = get_input_parameter_value(argv, '-t')
        if not websocket_client_type_param:
            websocket_client_type = WebsocketClientType.WEBSOCKET
        else:
            websocket_client_type = WebsocketClientType[websocket_client_type_param.upper()]
        publisher_name = get_input_parameter_value(argv, '-n')
        url_websocket_server = get_input_parameter_value(argv, '-a')
        # recover the topic name to publish
        event_to_publish = get_input_parameter_value(argv, '-e')
        event_to_listening = get_input_parameter_value(argv, '-s')
        message = get_input_parameter_value(argv, '-m')
        message_count = int(get_input_parameter_value(argv, '-c'))
        log_directory = get_input_parameter_value(argv, '-l')
        debug = ('-v' in argv)

        start_websocket_client(publisher_name=publisher_name,
                               url_websocket_server=url_websocket_server,
                               event_to_publish=event_to_publish,
                               event_to_listening=event_to_listening,
                               message=message,
                               message_count=message_count,
                               log_directory=log_directory,
                               websocket_client_type=websocket_client_type,
                               debug=debug)


if __name__ == '__main__':
    start(sys.argv)
