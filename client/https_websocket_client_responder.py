#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os
import sys



# including some directories to PATH 
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'message_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from client.https_websocket_client_base import HttpsWebSocketClientBase
from common_lib.common_base import valid_mandatory_parameters, get_input_parameter_value
from common_lib.websocket_client_type import WebsocketClientType
from common_lib.common_error import BadUserInputError
from client.responder_base import ResponderBase


class HttpsWebsocketClientResponder(HttpsWebSocketClientBase, ResponderBase):
    # Constructor
    def __init__(self,
                 url_websocket_server: str,
                 responder_name: str,
                 event_to_response: str,
                 event_to_listening: str,
                 ca_cert_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 websocket_client_type: WebsocketClientType = WebsocketClientType.WEBSOCKET,
                 debug: bool = False):
        HttpsWebSocketClientBase.__init__(self=self,
                                          url_websocket_server=url_websocket_server,
                                          event_to_publish=event_to_response,
                                          event_to_listening=event_to_listening,
                                          ca_cert_path=ca_cert_path,
                                          certfile_path=certfile_path,
                                          keyfile_path=keyfile_path,
                                          on_message_callback=self.on_message,
                                          on_start_handler=self.on_start,
                                          on_stop_handler=self.on_stop,
                                          websocket_client_type=websocket_client_type,
                                          debug=debug)
        ResponderBase.__init__(self=self, responder_method_callback=self._publish_message, responder_name=responder_name)

    def on_start(self):
        self._connect()

    def on_stop(self):
        self._disconnect()

    def on_message(self, message):
        self._on_message(string_message_json=message, message_origin=self.event_to_listening() )
        if self._debug:
            print("Received message ==> " + self.event_to_listening()+ " - " + str(message))



def start_websocket_client(publisher_name,
                           url_websocket_server,
                           event_to_response,
                           event_to_listening,
                           websocket_client_type,
                           debug):
    ca_cert_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        'certs',
        'mqtt-ca.crt')

    certfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-responder.crt")

    keyfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-responder.key")

    mqtt_client_responder = HttpsWebsocketClientResponder(url_websocket_server=url_websocket_server,
                                                          responder_name=publisher_name,
                                                          event_to_response=event_to_response,
                                                          event_to_listening=event_to_listening,
                                                          ca_cert_path=ca_cert_path,
                                                          certfile_path=certfile_path,
                                                          keyfile_path=keyfile_path,
                                                          websocket_client_type=websocket_client_type,
                                                          debug=debug)

    mqtt_client_responder.start()


def start(argv):
    if '-?' in argv:
        print("""Flask SocketIO Client to send a blast of messages to stress test Usage: python3 
        client_https_websocket.py -n publisher_name -a url_websocket_server -e event_name_to_publish 
        [-s topic_to_subscribe] [-t websocket_client_type] [-?] [-v] 

                -n : name of publisher, that will be sent inside the messages.
                -a : url address of https websocket server
                -e : event name to send the messages
                -s : event name to receive messages from websocket server, this is optional.
                -t : websocket client connection type, values are SOCKEIO or WEBSOCKET, if not set, will assume WEBSOCKET
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    elif valid_mandatory_parameters(argv, ['-n', '-a', '-e']):
        raise BadUserInputError(
            "Input error. To run, call as python3 client_https_websocket.py -n publisher_name -e event_name_to_publish "
            "[-s topic_to_subscribe] [-t websocket_client_type] [-?] [-v]")
    else:
        websocket_client_type_param = get_input_parameter_value(argv, '-t')
        if not websocket_client_type_param:
            websocket_client_type = WebsocketClientType.WEBSOCKET
        else:
            websocket_client_type = WebsocketClientType[websocket_client_type_param.upper()]
        publisher_name = get_input_parameter_value(argv, '-n')
        url_websocket_server = get_input_parameter_value(argv, '-a')
        # recover the topic name to publish
        event_to_response = get_input_parameter_value(argv, '-e')
        event_to_listening = get_input_parameter_value(argv, '-s')
        debug = ('-v' in argv)

        start_websocket_client(publisher_name=publisher_name,
                               url_websocket_server=url_websocket_server,
                               event_to_response=event_to_response,
                               event_to_listening=event_to_listening,
                               websocket_client_type=websocket_client_type,
                               debug=debug)


if __name__ == '__main__':
    start(sys.argv)
