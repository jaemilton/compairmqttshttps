#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import time
import json
import paho.mqtt.client as mqtt
import threading
from common_lib.common_base import CommonBase

class MqttClientBase(CommonBase):
    # Constructor
    def __init__(self,
                 mqtt_server: str,
                 mqtt_server_port: int,
                 topic_to_publish: str,
                 topic_to_subscribe: str,
                 ca_certs_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 on_message_callback,
                 on_start_handler,
                 on_stop_handler=None,
                 debug: bool = False):
        CommonBase.__init__(self, on_start_handler=on_start_handler, on_stop_handler=on_stop_handler)
        self.__subscribed = False
        self._mqtt_server = mqtt_server
        self._mqtt_server_port = mqtt_server_port
        self._topic_to_publish = topic_to_publish
        self._topic_to_subscribe = topic_to_subscribe
        self._debug = debug
        self.__ca_certs_path = ca_certs_path
        self.__certfile_path = certfile_path
        self.__keyfile_path = keyfile_path

        self.__mqtt_client = mqtt.Client()

        # assign callback functions
        self.__mqtt_client.on_message = on_message_callback
        self.__mqtt_client.on_subscribe = self.__on_subscribe
        self.__mqtt_client.on_connect = self.__on_connect
        if self._debug:
            self.__mqtt_client.on_log = self.__on_log
            self.__mqtt_client.on_publish = self.__on_publish

        # Loads TLS cetificates.
        self.__mqtt_client.tls_set(
            ca_certs=self.__ca_certs_path,
            certfile=self.__certfile_path,
            keyfile=self.__keyfile_path)

    def _subscribe(self):
        if self._topic_to_subscribe:
            sub = self.__mqtt_client.subscribe(topic=self._topic_to_subscribe, qos=0)
            if self._topic_to_subscribe and mqtt.MQTT_ERR_SUCCESS != sub.index(0):
                print("Error on subscribe topic " + self._topic_to_subscribe)
            while not self.__subscribed:
                print("Waiting for subscription on topic " + self._topic_to_subscribe)
                time.sleep(1)

    def _on_start_mqtt_loop(self):
        self.__mqtt_client.connect(host=self._mqtt_server, port=self._mqtt_server_port)
        __mqtt_loop_thead = threading.Thread(target=self.__mqtt_client.loop_forever)
        __mqtt_loop_thead.daemon = True
        __mqtt_loop_thead.start()

    def _disconnect(self):
        self.__mqtt_client.disconnect()


    def __on_log(self, client, userdata, level, buf):
        print(buf)

    def __on_connect(self, client, userdata, flags, rc):
        print("Connected rc: " + str(rc))

    def __on_subscribe(self, client, userdata, mid, granted_qos):
        print("Subscribed: rc: " + str(mid) + " - topic: " + self._topic_to_subscribe)
        self.__subscribed = True

    def __on_publish(self, client, obj, mid):
        print("mid: " + str(mid))

    def _publish_message(self, message_id, message):
        rc = self.__mqtt_client.publish(self._topic_to_publish, json.dumps(message))
        if not (rc.rc == mqtt.MQTT_ERR_SUCCESS):
            print("error to send message {0}".format(message_id))


