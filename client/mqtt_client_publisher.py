#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys
import os
import threading
import datetime

# including some directories to PATH 
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'message_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

# including some path directories
from common_lib.common_base import valid_mandatory_parameters
from common_lib.common_base import get_input_parameter_value
from client.mqtt_client_base import MqttClientBase
from common_lib.common_error import BadUserInputError
from client.publisher_base import PublisherBase
import paho.mqtt.client as mqtt
from message_lib.message_util import MessageUtil


class MqttClientPublisher(MqttClientBase, PublisherBase):

    # Constructor
    def __init__(self,
                 mqtt_server: str,
                 mqtt_server_port: int,
                 client_name: str,
                 topic_to_publish: str,
                 topic_to_subscribe: str,
                 message: str,
                 ca_certs_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 message_count: int = 1,
                 log_directory: str = None,
                 debug: bool = False):
        MqttClientBase.__init__(self,
                                mqtt_server=mqtt_server,
                                mqtt_server_port=mqtt_server_port,
                                topic_to_publish=topic_to_publish,
                                topic_to_subscribe=topic_to_subscribe,
                                ca_certs_path=ca_certs_path,
                                certfile_path=certfile_path,
                                keyfile_path=keyfile_path,
                                on_message_callback=self.on_message,
                                on_start_handler=self.on_start,
                                on_stop_handler=self.on_stop,
                                debug=debug)

        if not log_directory:
            log_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "logs")

        csv_file_name = "{0}_{1}.csv".format(type(self).__name__,
                                                    datetime.datetime.now().strftime(
                                                        "%Y%m%d%H%M%S"), 'w', newline='')

        PublisherBase.__init__(self=self,
                               message_count=message_count,
                               log_file_name=csv_file_name,
                               log_directory=log_directory,
                               client_name=client_name,
                               publisher_method_callback=self._publish_message,
                               message=message,
                               debug=debug)


    def on_start(self):
        self._on_start_mqtt_loop()
        self._subscribe()
        self._on_start()

    def on_stop(self):
        self._disconnect()
        self._on_stop()
        
    def on_message(self, client, obj, message):
        self._on_message(event_name=message.topic, message=message.payload)
        if self.all_response_messages_received():
            print("All " + str(self._message_count) + " sent were responded.")
            self.stop()


def start_mqtt_client(mqtt_server, 
                        mqtt_server_port, 
                        publisher_name, 
                        topic_to_publish, 
                        topic_to_subscribe, 
                        message,
                        message_count, 
                        log_directory,
                        debug):
    ca_certs_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        "certs",
        "mqtt-ca.crt")

    certfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-publisher.crt")

    keyfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-publisher.key")

    mqtt_client_publisher = MqttClientPublisher(mqtt_server=mqtt_server,
                                                mqtt_server_port=mqtt_server_port,
                                                client_name=publisher_name,
                                                topic_to_publish=topic_to_publish,
                                                topic_to_subscribe=topic_to_subscribe,
                                                message=message,
                                                ca_certs_path=ca_certs_path,
                                                certfile_path=certfile_path,
                                                keyfile_path=keyfile_path,
                                                message_count=message_count,
                                                log_directory = log_directory,
                                                debug=debug)

    mqtt_client_publisher.start()


def start(argv):
    if '-?' in argv:
        print("""Client Mqtt to send a blast of messages for stress test Usage: 
        python3 mqtt_client_publisher.py -h mqtt_server_name -p mqtt_server_port 
        -n publisher_name -t topic_to_publish -m message [-s topic_to_subscribe] [-?] [-c message_count] 
        [-l log_directory] [-v] 

                -h : mqtt server hostname or ip
                -p : mqtt server port
                -n : name of publisher, that will be sent inside the messages.
                -t : topic name to publish the messages
                -s : topic name to subscribe on mqtt server to receive message responses, this is optional.
                -c : number of messages to me sent on stress test
                -m : message that will be sent
                -l : Optional, log directory, if not defined, will create a folter logs on current directory
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    elif valid_mandatory_parameters(argv, ['-h', '-p', '-n', '-t', '-m']):
        raise BadUserInputError(
            "Input error. To run, call as >>> "
            "python3 mqtt_client_publisher.py -h mqtt_server_name -p mqtt_server_port "
            "-n publisher_name -t topic_to_publish -m message [-s topic_to_subscribe] [-?] [-c message_count] "
            "[-l log_directory] [-v]")
    else:
        mqtt_server = get_input_parameter_value(argv, '-h')
        mqtt_server_port = int(get_input_parameter_value(argv, '-p'))
        publisher_name = get_input_parameter_value(argv, '-n')
        topic_to_publish = get_input_parameter_value(argv, '-t')
        topic_to_subscribe = get_input_parameter_value(argv, '-s')
        message = get_input_parameter_value(argv, '-m')
        message_count = int(get_input_parameter_value(argv, '-c'))
        log_directory = get_input_parameter_value(argv, '-l')
        debug = ('-v' in argv)

        start_mqtt_client(mqtt_server=mqtt_server,
                        mqtt_server_port=mqtt_server_port,
                        publisher_name=publisher_name,
                        topic_to_publish=topic_to_publish,
                        topic_to_subscribe=topic_to_subscribe, 
                        message=message,
                        message_count=message_count, 
                        log_directory=log_directory,
                        debug=debug)


if __name__ == '__main__':
    start(sys.argv)
