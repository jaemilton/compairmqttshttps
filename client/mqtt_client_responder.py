#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys
import os

# including some directories to PATH 
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'message_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))


# including some path directories
from common_lib.common_base import valid_mandatory_parameters
from common_lib.common_base import get_input_parameter_value
from common_lib.common_time_control import get_microseconds_between_timestamps
from client.mqtt_client_base import MqttClientBase
from message_lib.message_util import MessageUtil
from common_lib.common_error import BadUserInputError
from client.responder_base import ResponderBase
import paho.mqtt.client as mqtt


class MqttClientResponder(MqttClientBase, ResponderBase):

    # Constructor
    def __init__(self,
                 mqtt_server: str,
                 mqtt_server_port: int,
                 responder_name: str,
                 topic_to_publish: str,
                 topic_to_subscribe: str,
                 ca_certs_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 debug: bool = False):
        MqttClientBase.__init__(self,
                                mqtt_server=mqtt_server,
                                mqtt_server_port=mqtt_server_port,
                                topic_to_publish=topic_to_publish,
                                topic_to_subscribe=topic_to_subscribe,
                                ca_certs_path=ca_certs_path,
                                certfile_path=certfile_path,
                                keyfile_path=keyfile_path,
                                on_start_handler=self.on_start,
                                on_message_callback=self.on_message,
                                debug=debug)
        ResponderBase.__init__(self=self, responder_method_callback=self._publish_message, responder_name=responder_name)


    def on_start(self):
        self._on_start_mqtt_loop()
        self._subscribe()

   
    def on_message(self, client, userdata, message):
        self._on_message(string_message_json=message.payload, message_origin=message.topic)
        if self._debug:
            print("Received message ==> " + message.topic + " " + str(message.qos) + " " + str(message.payload))
        

def start_mqtt_responder(mqtt_server, mqtt_server_port, responder_name, topic_to_subscribe, topic_to_response, debug):
    ca_certs_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        "certs",
        "mqtt-ca.crt")

    certfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-responder.crt")

    keyfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "configs",
        "certs",
        "mqtt-responder.key")

    mqtt_client_responder = MqttClientResponder(mqtt_server=mqtt_server,
                                                mqtt_server_port=mqtt_server_port,
                                                responder_name=responder_name,
                                                topic_to_subscribe=topic_to_subscribe,
                                                topic_to_publish=topic_to_response,
                                                ca_certs_path=ca_certs_path,
                                                certfile_path=certfile_path,
                                                keyfile_path=keyfile_path,
                                                debug=debug)

    mqtt_client_responder.start()


def start(argv):
    if '-?' in argv:
        print("""
                Client responder mqtt to respond each message sent from publisher
                Usage: python3 mqtt_client_responder.py -h mqtt_server_name -p mqtt_server_port -n responser_name -s topic_to_subscribe -r topic_to_respond [-?] [-v]

                -h : mqtt server hostname or ip
                -p : mqtt server port
                -n : name of publisher, that will be sent inside the messages.
                -r : topic name to publish the response messages
                -s : topic name to subscribe on mqtt server to receive message client messages
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    elif valid_mandatory_parameters(argv, ['-h', '-p', '-n', '-s', '-r']):
        raise BadUserInputError(
            "Input error. To run, call python3 mqtt_client_responder.py -h mqtt_server_name -p mqtt_server_port -n "
            "responser_name -s topic_to_subscribe -r topic_to_respond [-?] [-v]")
    else:
        mqtt_server = get_input_parameter_value(argv, '-h')
        mqtt_server_port = int(get_input_parameter_value(argv, '-p'))
        responder_name = get_input_parameter_value(argv, '-n')
        # recover the topic name to publish
        topic_to_response = get_input_parameter_value(argv, '-r')
        topic_to_subscribe = get_input_parameter_value(argv, '-s')
        debug = ('-v' in argv)

        start_mqtt_responder(mqtt_server, mqtt_server_port, responder_name, topic_to_subscribe, topic_to_response,
                             debug)


if __name__ == '__main__':
    start(sys.argv)
