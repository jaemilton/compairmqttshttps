Write-Host "Checking for elevated permissions..."
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
[Security.Principal.WindowsBuiltInRole] "Administrator")) {
  Write-Warning "Insufficient permissions to run this script. Open the PowerShell console as an administrator and run this script again."
  Exit
}


Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#choco install python3 --params "/InstallDir:C:\your\install\path"
choco install python3 -y

where.exe python3
Write-Output "#### Checking Python verstion"
python3 ./check_python_version.py

if(!$?) {
  # True, last operation succeeded
  Exit
}

Write-Output "#### Installing python3 library paho.mqtt"
py -m pip install paho-mqtt
Write-Output  ""
