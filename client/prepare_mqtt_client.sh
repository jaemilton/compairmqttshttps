#!/bin/bash
echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path

IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi

echo "#### Installing  python3, python3-pip"
apt-get install python3 python3-pip
echo ""

python3 $dir_path/check_python_version.py
## take some decision ## 
if [ "$?" -ne 0 ] 
then
    exit
fi

echo "#### Installing python3 library paho.mqtt"
pip3 install paho-mqtt==1.5.1
echo ""
