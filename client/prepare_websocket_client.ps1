Write-Host "Checking for elevated permissions..."
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
[Security.Principal.WindowsBuiltInRole] "Administrator")) {
  Write-Warning "Insufficient permissions to run this script. Open the PowerShell console as an administrator and run this script again."
  Exit
}


Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#choco install python3 --params "/InstallDir:C:\your\install\path"
choco install python3 -y --params "/InstallDir:C:\Python39"

where.exe python
Write-Output "#### Checking Python verstion"
$exeName        = "check_python_version.py"
$scriptFolder   = Split-Path -Parent $MyInvocation.MyCommand.Path
$exeFullPath    = Join-Path -Path $scriptFolder -ChildPath $exeName

C:\Python39\python.exe $exeFullPath 

if(!$?) {
  # True, last operation succeeded
  Exit
}


Write-Host "#### Installing python3 library python-socketio[client]"
pip3 install --upgrade pip
pip3 install python-socketio==5.0.4
pip3 install python-engineio==4.0.0
pip3 install requests==2.25.1
pip3 install certifi==2020.12.5
pip3 install websocket-client==0.58.0
Write-Host ""

