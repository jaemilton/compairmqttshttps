#!/bin/bash
echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path

IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi


echo "#### Installing  python3, python3-pip"
apt-get install python3 python3-pip

python3 $dir_path/check_python_version.py
status=$?
## take some decision ## 
if [ $status -ne 0 ] 
then
   exit
fi

echo ""
echo "#### Installing python3 library python-socketio[client]"
pip3 install --upgrade pip
pip3 install python-socketio==5.0.4
pip3 install python-engineio==4.0.0
pip3 install requests==2.25.1
pip3 install certifi==2020.12.5
pip3 install websocket-client==0.58.0
echo ""

