#!/usr/bin/python3
# -*- coding: UTF-8 -*-


import threading
from typing import Any, List, Tuple
from common_lib.common_base import TIMESTAMP_FORMAT
import os
from datetime import date, datetime
import sys
import time

from message_lib.message_util import MessageUtil
from common_lib.common_base import TIMESTAMP_FORMAT
from common_lib.common_time_control import get_microseconds_between_timestamps
from common_lib.common_csv import CommonCsv
from common_lib.common_time_control import CommonTimeControl
from message_lib.message_error import InvalidMessageFormatError
from message_lib.message_error import InvalidParameterError


class PublisherBase(CommonCsv, CommonTimeControl):
    #received_timestamp, response_timestamp
    __LOG_HEAD = ["counter", "message_source", "sent_timestamp", "message", "original_event", 
                  "message_responder", "response_event", "response_message", 
                  "microseconds_reveived_message", "microseconds_reveived_response",
                  "total_microseconds", "delta_microseconds", "accumulated_delta_microseconds"]

    __lock = threading.Lock()
    _received_response_pending_to_log = []
    
    def __init__(self,
                 message_count: int,
                 log_file_name: str,
                 log_directory: str,
                 client_name: str,
                 publisher_method_callback,
                 message: str,
                 on_publish_finished = None,
                 debug: bool = False) :
        if publisher_method_callback is None:
            raise InvalidParameterError("The publisher_method_callback must be provided")
        self._client_name = client_name
        CommonCsv.__init__(self=self, csv_header=self.__LOG_HEAD, log_file_name=log_file_name, log_directory=log_directory)
        CommonTimeControl.__init__(self=self, auto_start=False)
        self.publisher_method_callback = publisher_method_callback
        self.__message = message
        self._thread_running = False
        self._message_count = message_count
        self._received_messages_count = 0
        self.__debug = debug
        self.__thead_publisher = threading.Thread(target=self.publisher)
        self.__on_publish_finished = on_publish_finished
        self.__thead_log = threading.Thread(target=self.__prepare_log_to_write)
    
    def publisher(self):
        self._thread_running = True
        self._start_time()
        print("Start sending " + str(self._message_count) + " messages.")
        for counter in range(1, self._message_count + 1):
            if not self._thread_running:
                if self.__debug:
                    print("Publisher thread stopped")
                break
            message = MessageUtil(message_source=self._client_name, counter=counter)
            self.publisher_method_callback(
                message_id=counter,
                message=message.create_json_sent_message(content=self.__message)
            )
        print("Publisher thread finished")
        if self.__on_publish_finished:
            self.__on_publish_finished()
        

    def _on_start(self) -> None:
        self.__thead_publisher.daemon = True
        self.__thead_publisher.start()
        self.__thead_log.daemon = True
        self.__thead_log.start()

    def _on_stop(self) -> None:
        num_rows_to_prepare = 0

        while True:
            with self.__lock:
                num_rows_to_prepare = len(self._received_response_pending_to_log)
            if num_rows_to_prepare == 0:
                break
            print("waiting " + str(num_rows_to_prepare) + " logs to be prepared to be written")
            time.sleep(1)

        self.stop_csv_writting()
        self._thread_running = False
  

    def _on_message(self, event_name, message) -> None:
        date_time_response:datetime = datetime.now()
        with self.__lock:
            self._update_time()
            self._received_response_pending_to_log.append((event_name, 
                                                            message, 
                                                            date_time_response, 
                                                            self.get_total_time_microseconds(),
                                                            self.get_delta_time_microseconds(),
                                                            self.get_accumulated_delta_time_microseconds()
                                                            )
                                                        )
        self._received_messages_count += 1

        if self.__debug:
            print("Received message ==> event: " + event_name + ", message: " + str(message))

        

        if (self._received_messages_count % 10) == 0:
            print("received " + str(self._received_messages_count) + " messages",  end = "\r")

    def all_response_messages_received(self) -> bool:
        return self._message_count == self._received_messages_count

    def get_rows_to_write_log(self, data_tuple:Tuple[str, str, datetime, int, int]) -> List[Any]:
        log_row = []
        (event_name, message, date_time_response, total_microseconds, delta_microseconds, accumulated_delta_microseconds) = data_tuple

        # save the message response on log file
        message_util = MessageUtil(string_message_json=message)
        message_util.set_attribute("response_event", event_name)
        message_util.set_attribute("response_timestamp", TIMESTAMP_FORMAT.format(timestamp=datetime.timestamp(date_time_response)))

        sent_timestamp = float(message_util.get_attribute_value("sent_timestamp"))
        received_timestamp = float(message_util.get_attribute_value("received_timestamp"))
        response_timestamp = float(message_util.get_attribute_value("response_timestamp"))

        microseconds_reveived_message = get_microseconds_between_timestamps(start_timestamp=sent_timestamp, end_timestamp=received_timestamp)
        microseconds_reveived_response = get_microseconds_between_timestamps(start_timestamp=received_timestamp, end_timestamp=response_timestamp)

        message_util.set_attribute("microseconds_reveived_message", microseconds_reveived_message)
        message_util.set_attribute("microseconds_reveived_response", microseconds_reveived_response)
        message_util.set_attribute("total_microseconds", total_microseconds)
        message_util.set_attribute("delta_microseconds", delta_microseconds)
        message_util.set_attribute("accumulated_delta_microseconds", accumulated_delta_microseconds)

        if  microseconds_reveived_response < 0:
            raise InvalidMessageFormatError(
                "message {0} - response_date_time  => {1} smaller then server_received_datetime => {2}"
                .format(message_util.get_attribute_value("counter"), 
                        date.fromtimestamp(response_timestamp),
                        date.fromtimestamp(received_timestamp)
                         ))

        for column in self.__LOG_HEAD:
            log_row.append(message_util.get_attribute_value(column))

        return log_row 
        
    def __prepare_log_to_write(self) -> None:
        while self._thread_running:
            num_rows_to_prepare = len(self._received_response_pending_to_log)
            if num_rows_to_prepare > 0:
                for received_message in self._received_response_pending_to_log[0:num_rows_to_prepare]:
                    log_row = self.get_rows_to_write_log(data_tuple=received_message)
                    self.append_log(log_row=log_row)
                with self.__lock:
                    del self._received_response_pending_to_log[0:num_rows_to_prepare]
            time.sleep(1)
    
 