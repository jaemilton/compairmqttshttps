
from datetime import datetime
from message_lib.message_util import MessageUtil
from message_lib.message_error import InvalidMessageFormatError
from message_lib.message_error import InvalidParameterError
from common_lib.common_time_control import get_microseconds_between_timestamps

class ResponderBase(object):
    def __init__(self, 
                    responder_name:str,
                    responder_method_callback) -> None:
        
        if responder_method_callback is None:
            raise InvalidParameterError("The responder_method_callback must be provided")
        self.responder_method_callback = responder_method_callback
        self.__received_messages_count = 0
        self._responder_name = responder_name

   

    def _on_message(self, string_message_json: str, message_origin:str) -> None:
        date_time_reception = datetime.now()
        self.__received_messages_count += 1
       
        message_util = MessageUtil(string_message_json=string_message_json, 
                                       reception_timestamp=datetime.timestamp(date_time_reception),
                                       origin=message_origin)
        message_counter=message_util.get_attribute_value("counter")

        # respond each message to response topic
        self.responder_method_callback(
            message_id=message_counter,
            message=message_util.create_json_response_message(
                responder=self._responder_name,
                content="response message " + str(message_counter)
            ))

        if (self.__received_messages_count % 10) == 0:
            print("responded " + str(self.__received_messages_count) + " messages",  end = "\r")