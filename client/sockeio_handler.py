#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
import requests
import socketio
from engineio.payload import Payload

from ws_base_handler import WsBaseHandler

class SocketioHandler(WsBaseHandler):
    def __init__(self,
                 event_to_publish: str,
                 url_websocket_server: str,
                 ca_cert_path: str,
                 certfile_path: str,
                 keyfile_path: str,
                 event_to_listening: str,
                 on_message_callback,
                 running_function_callback, 
                 debug: bool = False) -> None:
        WsBaseHandler.__init__(self=self, 
                                event_to_publish=event_to_publish,
                                url_websocket_server=url_websocket_server,
                                event_to_listening=event_to_listening,
                                on_message_callback=on_message_callback,
                                running_function_callback=running_function_callback,
                                debug=debug)
        self._num_confirmed_sent_message = 0
        Payload.max_decode_packets = 5000000
        verify_ssl = self.url_websocket_server.startswith("https://") or self.url_websocket_server.startswith("wss://")
        # Set the Certificate Authority path file to be able to get a ssl connection
        os.environ['REQUESTS_CA_BUNDLE'] = ca_cert_path

        #set the certificate and key
        http_session = requests.Session()
        http_session.cert = (certfile_path, keyfile_path)

        self.__sio_client_publisher = socketio.Client(ssl_verify=verify_ssl, logger=debug, engineio_logger=debug, http_session=http_session)
        # Configures event listening for publisher
        self.__sio_client_publisher.on(event='connect', handler=self.__on_connect_publisher)
        self.__sio_client_publisher.on(event='disconnect', handler=self.__on_disconnect_publisher)
        self.__sio_client_publisher.on(event='connect_error', handler=self.__on_connect_error)

        self.__sio_client_subscriber = socketio.Client(ssl_verify=verify_ssl, logger=debug, engineio_logger=debug)

        # Configures event listening  for publisher
        self.__sio_client_subscriber.on(event=self.event_to_listening, handler=self._on_message_callback)
        self.__sio_client_subscriber.on(event='connect', handler=self.__on_connect_subscriber)
        self.__sio_client_subscriber.on(event='disconnect', handler=self.__on_disconnect_subscriber)
        self.__sio_client_subscriber.on(event='connect_error', handler=self.__on_connect_error)


    def __on_connect_publisher(self):
        self.set_publisher_connected(True)
        print("Client publisher connected. Listing to events " + self.event_to_listening)

    def __on_disconnect_publisher(self):
        self.set_publisher_connected(False)
        print("Client publisher disconnected.")


    def __on_connect_error(self, error):
        print("Client connection error. ==> " + str(error))
   

    def __on_connect_subscriber(self):
        self.set_subscriber_connected(True)
        print("Client responder connected. Listing to events " + self.event_to_listening)

    def __on_disconnect_subscriber(self):
        self.set_subscriber_connected(False)
        print("Client responder disconnected.")

    # uncomment for line_profiler
    # @profile
    def publish_message(self, message_id, message):
        try:
            self.__emit(data=(self.event_to_publish, message))
            self._sent_messages_count += 1
        except Exception as ex:
            template = "Error to send message {0}. Exception occurred {1}. Arguments:\n{2!r}"
            message = template.format(message_id, type(ex).__name__, ex.args)
            print(message)

    def connect_publisher(self):
        if not self.is_publisher_connected():
            self.__connect(self.__sio_client_publisher)
            self.wait_for_connection(connection_check_function=self.is_publisher_connected)
    
    def connect_subscriber(self):
        if not self.is_subscriber_connected():
            self.__connect(self.__sio_client_subscriber)
            self.wait_for_connection(connection_check_function=self.is_subscriber_connected)

    def __connect(self, sio_client: socketio.Client):
        sio_client.connect(url=self._url_websocket_server)


    def disconnect_publisher(self):
        if self.is_publisher_connected():
            self.__sio_client_publisher.disconnect()

    def disconnect_subscriber(self):
        if self.is_subscriber_connected():
            self.__sio_client_subscriber.disconnect()

    def __on_sent_message_confirmed(self, data):
        self._num_confirmed_sent_message += 1
        if (self._num_confirmed_sent_message % 50) == 0 or self._debug:
            print("messages sent " + str(self._num_confirmed_sent_message) + " confirmed.")


    def __emit(self, data):
        # emit a message to server
        self.wait_for_connection(connection_check_function=self.is_publisher_connected)

        message_sent_confirm_callback = None
        if self._debug:
            message_sent_confirm_callback = self.__on_sent_message_confirmed

        self.__sio_client_publisher.emit(event='event', data=data, callback=message_sent_confirm_callback)