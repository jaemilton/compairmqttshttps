#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import time
class WsBaseHandler(object):
    def __init__(self,
                event_to_publish: str,
                 url_websocket_server: str,
                 event_to_listening: str,
                 on_message_callback,
                 running_function_callback, 
                 debug: bool = False) -> None:
                 
        self._on_message_callback = on_message_callback
        self._running_function_callback = running_function_callback
        self._url_websocket_server = url_websocket_server
        self._sent_messages_count = 0
        self.__publisher_connected = False
        self.__subscriber_connected = False
        self.event_to_publish = event_to_publish
        self.event_to_listening = event_to_listening

    def wait_for_connection(self, connection_check_function):
        while not connection_check_function() :
            if not self._running_function_callback():
                return
            print("Waiting for connection")
            time.sleep(1) # give the reconnect task time to start up 

    def is_publisher_connected(self) -> bool:
        return self.__publisher_connected

    def is_subscriber_connected(self) -> bool:
        return self.__subscriber_connected

    def set_publisher_connected(self, value: bool) -> None:
        self.__publisher_connected = value

    def set_subscriber_connected(self, value: bool) -> None:
        self.__subscriber_connected = value