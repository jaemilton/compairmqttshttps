#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from enum import Enum

class ClientType(Enum):
     PUBLISHER = 1
     SUBSCRIBER = 2
     BOTH = 2