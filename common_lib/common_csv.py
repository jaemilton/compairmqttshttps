#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import csv
import os
import threading
import time
from typing import Any, Iterable, List
from common_lib.common_error import BadUserInputError

class CommonCsv(object):
    
    WRITE_INTERVAL_SECONDS = 3
    __lock = threading.Lock()
    
    def __init__(self,
                 csv_header,
                 log_file_name,
                 log_directory
                 ) -> None:
        
        if not log_file_name:
            raise BadUserInputError("Log file name must be specified.")

        self.__csv_file_name = log_file_name
        self.__log_directory = log_directory

        if not self.__log_directory:
            raise BadUserInputError("Log directory name must be specified.")

        if not (os.path.exists(self.__log_directory)):
            os.mkdir(self.__log_directory)

        self.__csv_file = open(file=os.path.join(self.__log_directory, self.__csv_file_name),
                               mode='w',
                               newline='')

        self.__csv_writer = csv.writer(self.__csv_file, delimiter=';',
                                       quotechar='"', quoting=csv.QUOTE_MINIMAL)


        # creates the csv log head
        self._log_rows = []
        self._written_log_count = 0
        self._expected_written_log = 0
        self.append_log(csv_header)
        self.__thead_log = threading.Thread(target=self.create_log)
        self.__thead_log.daemon = True
        self.__thead_log.start()


    def stop_csv_writting(self): 
        while not (self._written_log_count == self._expected_written_log):
            print("Waiting logs to be written.")
            time.sleep(self.WRITE_INTERVAL_SECONDS)
        self._csv_running = False
       

    def append_log(self, log_row) -> None:
        with self.__lock:
            self._log_rows.append(log_row)
            self._expected_written_log += 1

    def append_range_log(self, log_rows: List[List[Any]]) -> None:
        with self.__lock:
            self._log_rows.extend(log_rows)
            self._expected_written_log += len(log_rows)

    def create_log(self) -> None:
        self._csv_running = True
        while self._csv_running:
            num_rows_to_write = len(self._log_rows)
            if num_rows_to_write > 0:
                self.__csv_writer.writerows(self._log_rows[0:num_rows_to_write])
                self.__csv_file.flush()
                self._written_log_count += num_rows_to_write
                with self.__lock:
                    del self._log_rows[0:num_rows_to_write]
            time.sleep(self.WRITE_INTERVAL_SECONDS)

        
      