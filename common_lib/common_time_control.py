#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import threading
from common_lib.common_error import BadUserInputError
from datetime import datetime
from common_lib.common_timing import micros

def get_microseconds_between_timestamps(start_timestamp: float, end_timestamp: float):
    """
    Calculate the diferrence in microseconds between two timestamps
    """
    if not start_timestamp or not end_timestamp:
        raise BadUserInputError("start_timestamp and end_timestamp must be provided.")
    
    return (datetime.fromtimestamp(end_timestamp) - datetime.fromtimestamp(start_timestamp)).microseconds

class CommonTimeControl(object):
    __lock = threading.Lock()

    def __init__(self, auto_start=True) -> None:
        self.__first_microssecond_update = None
        if auto_start:
            self._start_time()
        else:
            self.__last_microsecond_update = self.__first_microssecond_update
            
        self.__total_time_microseconds = 0
        self.__delta_time_microseconds = 0
        self.__accumulated_delta_time_microseconds = 0

    def _start_time(self) -> None:
        if self.__first_microssecond_update is None:
            self.__first_microssecond_update = micros() #us
            self.__last_microsecond_update = self.__first_microssecond_update

    def _update_time(self, start_if_not_started=True ) -> bool:
        updated = False
        with self.__lock:
            if self.__first_microssecond_update is None and start_if_not_started:
                self._start_time()

            if self.__first_microssecond_update:
                current_microsecond_update = micros() #us
                
                #calculate delta time just from the second update forward
                if self.__total_time_microseconds > 0:
                    self.__delta_time_microseconds = current_microsecond_update - self.__last_microsecond_update
                    self.__accumulated_delta_time_microseconds += self.__delta_time_microseconds
                self.__total_time_microseconds = current_microsecond_update - self.__first_microssecond_update
                self.__last_microsecond_update = current_microsecond_update
                updated = True
        return updated
            


    def get_total_time_microseconds(self) -> int:
        with self.__lock:
            return int(self.__total_time_microseconds)

    def get_delta_time_microseconds(self) -> int:
        with self.__lock:
            return int(self.__delta_time_microseconds)

    def get_accumulated_delta_time_microseconds(self) -> int:
        with self.__lock:
            return int(self.__accumulated_delta_time_microseconds)


    def get_first_microssecond_update(self) -> int:
        with self.__lock:
            return None if not self.__first_microssecond_update else int(self.__first_microssecond_update)

    def get_last_microsecond_update(self) -> int:
        with self.__lock:
            return None if not self.__last_microsecond_update else int(self.__last_microsecond_update)
