#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from enum import Enum

class WebsocketClientType(Enum):
     SOCKEIO = 1
     WEBSOCKET = 2