#!/usr/bin/python3
# -*- coding: UTF-8 -*-

class MessageError(Exception):
    """Exception class from which every exception in this library will derive.
         It enables other projects using this library to catch all errors coming
         from the library with a single "except" statement
    """
    pass


class InvalidParameterError(MessageError):
    """A specific error"""
    pass

class InvalidMessageFormatError(MessageError):
    """A specific error"""
    pass
