#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from typing import Any, overload
from common_lib.common_base import TIMESTAMP_FORMAT
from datetime import datetime
import json

from message_lib.message_error import InvalidParameterError, InvalidMessageFormatError

     

class MessageUtil(object):
    __message_object = None

    def __init__(self, **kwargs) -> None:

        self.string_message_json = kwargs.get('string_message_json', None)
        self.origin = kwargs.get('origin', None)
        self.reception_timestamp = kwargs.get('reception_timestamp', None)

        if self.string_message_json and self.origin:
            self.__init_string_message_json_origin()

        elif self.string_message_json:
            self.__init_string_message_json_str()

        else:
            self.message_source = kwargs.get('message_source', None)
            self.counter = kwargs.get('counter', None)
            self.__init_message_source_counter()
 
    def __init_string_message_json_str(self) -> None:
        self.__fix_message_json_string()
        self.__message_object = json.loads(self.string_message_json)

    def __init_string_message_json_origin(self) -> None:
        self.__init_string_message_json_str()

        self.__message_object["original_event"] = self.origin
        if not self.reception_timestamp:
            self.reception_timestamp = datetime.timestamp(datetime.now())
        self.__message_object["received_timestamp"] = TIMESTAMP_FORMAT.format(timestamp=self.reception_timestamp)
    
    def __init_message_source_counter(self) -> None:

        self.__message_object = \
            {
                "counter": self.counter,
                "message_source": self.message_source
            }

    def __fix_message_json_string(self):
        if not self.string_message_json is None:
            # convert message to string
            if isinstance(self.string_message_json, bytes):
                self.string_message_json = self.string_message_json.decode("utf-8")
            elif not isinstance(self.string_message_json, str):
                raise InvalidMessageFormatError("The received message is not on expected format string or byte array")

            # cut-off all double backlash
            self.string_message_json = self.string_message_json.replace("\\", "")

            # if the string starts with quoute, cut it off
            if self.string_message_json[0] == '"':
                self.string_message_json = self.string_message_json[1:]

            # if the string ends with quoute, cut it off
            if self.string_message_json[-1] == '"':
                self.string_message_json = self.string_message_json[:-1]


    def set_attribute(self, name, value) -> None:
        self.__message_object[name] = value

    def get_attribute_value(self, name) -> object:
        return self.__message_object[name]

    def get_json_message(self) -> str:
        return json.dumps(self.__message_object)


    def create_json_sent_message(self, content: str) -> str:
        self.set_attribute("sent_timestamp",
            TIMESTAMP_FORMAT.format(timestamp=datetime.timestamp(datetime.now())))
            
        self.set_attribute("message", content)
        return self.get_json_message()

    def create_json_response_message(self, responder: str, content: str) -> str:
        self.set_attribute("message_responder", responder)
        self.set_attribute("response_message", content)
        return self.get_json_message()


