#!/bin/bash
echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path

EUID=$(id -u)
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "#### Installing  python3, python3-pip"
apt-get install python3 python3-pip -y
echo ""

echo "#### Installing python3 libraries setuptools psutil"
python3 -m pip install -U pip
pip3 install fabric==2.5.0
echo ""

echo "#### Preparing client mqtt"
$dir_path/client/prepare_mqtt_client.sh
echo ""

echo "#### Preparing client https websocket"
$dir_path/client/prepare_websocket_client.sh
echo ""