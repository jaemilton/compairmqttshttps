#!/bin/bash
IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi


apt install build-essential libncurses5-dev libpcap-dev git -y
git clone https://github.com/jaemilton/nethogs.git
cd ./nethogs
make libnethogs
cd ..
PLATFORM=$(python3 -c "import platform; print(platform.uname().machine)")
echo "libnethogs.so.$PLATFORM"
cp -f ./nethogs/src/libnethogs.so.* ./libnethogs/libnethogs.so.$PLATFORM
rm -rf ./nethogs