from typing import Tuple
from common_lib.common_timing import delay_microseconds, monotonic_time
from psutil import Process
import os

CLOCK_TICKS = os.sysconf("SC_CLK_TCK")
class CpuInfo(Process):
    def __init__(self, pid=None):
        super().__init__(pid=pid)
        self._last_sys_cpu_times = None
        self._last_proc_cpu_times = None
        self.cpu_count = len(self.cpu_affinity()) or 1
        print("CPU COUNT ==> " +  str(self.cpu_count))
        self.proc_stat_file = "/proc/" + str(self.pid) + "/stat"

    

    def __get_linux_cpu_times(self) -> Tuple[int, int]: #just Linux Compatible
        # Read first line from /proc/stat. It should start with "cpu"
        # and contains times spend in various modes by all CPU's totalled.
        with open(self.proc_stat_file) as procfile:
            cpustats = procfile.readline().split()

        user_time = float(cpustats[13]) / CLOCK_TICKS  # time spent in user space
        system_time = float(cpustats[14]) / CLOCK_TICKS # time spent in kernel space

        return user_time, system_time


    def cpu_percent(self, microseconds_interval: float =None):

        """Return a float representing the current process CPU
        utilization as a percentage.

        When *interval* is 0.0 or None (default) compares process times
        to system CPU times elapsed since last call, returning
        immediately (non-blocking). That means that the first time
        this is called it will return a meaningful 0.0 value.

        When *interval* is > 0.0 compares process times to system CPU
        times elapsed before and after the interval (blocking).

        In this case is recommended for accuracy that this function
        be called with at least 0.1 seconds between calls.

        A value > 100.0 can be returned in case of processes running
        multiple threads on different CPU cores.

        The returned value is explicitly NOT split evenly between
        all available logical CPUs. This means that a busy loop process
        running on a system with 2 logical CPUs will be reported as
        having 100% CPU utilization instead of 50%.

        Examples:

            >>> import psutil
            >>> p = psutil.Process(os.getpid())
            >>> # blocking
            >>> p.cpu_percent(interval=1)
            2.0
            >>> # non-blocking (percentage since last call)
            >>> p.cpu_percent(interval=None)
            2.9
            >>>
        """
        blocking = microseconds_interval is not None and microseconds_interval > 0.0
        if microseconds_interval is not None and microseconds_interval < 0:
            raise ValueError("interval is not positive (got %r)" % microseconds_interval)

        def timer():
            return monotonic_time() * self.cpu_count #return time.monotonic() * self.cpu_count

        if blocking:
            st1 = timer()
            pt1 = self.__get_linux_cpu_times() #self._proc.cpu_times()
            delay_microseconds(microseconds_interval) #time.sleep(seconds_interval)
            st2 = timer()
            pt2 = self.__get_linux_cpu_times() #elf._proc.cpu_times()
        else:
            st1 = self._last_sys_cpu_times
            pt1 = self._last_proc_cpu_times
            st2 = timer()
            pt2 = self.__get_linux_cpu_times() #self._proc.cpu_times()
            if st1 is None or pt1 is None:
                self._last_sys_cpu_times = st2
                self._last_proc_cpu_times = pt2
                return 0.0

        delta_proc = (pt2[0] - pt1[0]) + (pt2[1] - pt1[1])
        delta_time = st2 - st1
        # reset values for next call in case of interval == None
        self._last_sys_cpu_times = st2
        self._last_proc_cpu_times = pt2

        try:
            # This is the utilization split evenly between all CPUs.
            # E.g. a busy loop process on a 2-CPU-cores system at this
            # point is reported as 50% instead of 100%.
            overall_cpus_percent = (delta_proc / delta_time) * 100
        except ZeroDivisionError:
            # interval was too low
            return 0.0
        else:
            # Note 1:
            # in order to emulate "top" we multiply the value for the num
            # of CPU cores. This way the busy process will be reported as
            # having 100% (or more) usage.
            #
            # Note 2:
            # taskmgr.exe on Windows differs in that it will show 50%
            # instead.
            #
            # Note 3:
            # a percentage > 100 is legitimate as it can result from a
            # process with multiple threads running on different CPU
            # cores (top does the same), see:
            # http://stackoverflow.com/questions/1032357
            # https://github.com/giampaolo/psutil/issues/474
            single_cpu_percent = overall_cpus_percent / self.cpu_count
            return round(single_cpu_percent, 2)


