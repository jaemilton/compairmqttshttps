#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from enum import Enum

class MonitorType(Enum):
     CPU = 1
     NETWORK = 2
     BOTH = 3