#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import ctypes
import signal
import threading
import os
import platform
from common_lib.common_error import BadUserInputError

# Here are some definitions from libnethogs.h
# https://github.com/raboof/nethogs/blob/master/src/libnethogs.h
# Possible actions are NETHOGS_APP_ACTION_SET & NETHOGS_APP_ACTION_REMOVE
# Action REMOVE is sent when nethogs decides a connection or a process has died. There are two
# timeouts defined, PROCESSTIMEOUT (150 seconds) and CONNTIMEOUT (50 seconds). AFAICT, the latter
# trumps the former so we see a REMOVE action after ~45-50 seconds of inactivity.
class Action():
    SET = 1
    REMOVE = 2

    MAP = {SET: 'SET', REMOVE: 'REMOVE'}

class LoopStatus():
    """Return codes from nethogsmonitor_loop()"""
    OK = 0
    FAILURE = 1
    NO_DEVICE = 2

    MAP = {OK: 'OK', FAILURE: 'FAILURE', NO_DEVICE: 'NO_DEVICE'}

# The sent/received KB/sec values are averaged over 5 seconds; see PERIOD in nethogs.h.
# https://github.com/raboof/nethogs/blob/master/src/nethogs.h#L43
# sent_bytes and recv_bytes are a running total
class NethogsMonitorRecord(ctypes.Structure):
    """ctypes version of the struct of the same name from libnethogs.h"""
    _fields_ = (('record_id', ctypes.c_int),
                ('name', ctypes.c_char_p),
                ('pid', ctypes.c_int),
                ('uid', ctypes.c_uint32),
                ('device_name', ctypes.c_char_p),
                ('sent_bytes', ctypes.c_uint64),
                ('recv_bytes', ctypes.c_uint64),
                ('sent_kbs', ctypes.c_float),
                ('recv_kbs', ctypes.c_float),
                )




class NethogsUtil(object):

    # This is a Python 3 demo of how to interact with the Nethogs library via Python. The Nethogs
    # library operates via a callback. The callback implemented here just formats the data it receives
    # and prints it to stdout. This must be run as root (`sudo python3 python-wrapper.py`).
    # By Philip Semanchuk (psemanchuk@caktusgroup.com) November 2016
    # Copyright waived; released into public domain as is.

    # The code is multi-threaded to allow it to respond to SIGTERM and SIGINT (Ctrl+C).  In single-
    # threaded mode, while waiting in the Nethogs monitor loop, this Python code won't receive Ctrl+C
    # until network activity occurs and the callback is executed. By using 2 threads, we can have the
    # main thread listen for SIGINT while the secondary thread is blocked in the monitor loop.

    #######################
    # BEGIN CONFIGURATION #
    #######################

   
    LIBRARY_PATH = 'libnethogs'

    # LIBRARY_NAME has to be exact, although it doesn't need to include the full path.
    # The version tagged as 0.8.5 (download link below) builds a library with this name.
    # https://github.com/raboof/nethogs/archive/v0.8.5.tar.gz
    LIBRARY_NAME = 'libnethogs.so.' + platform.uname().machine

    # EXPERIMENTAL: Optionally, specify a capture filter in pcap format (same as
    # used by tcpdump(1)) or None. See `man pcap-filter` for full information.
    # Note that this feature is EXPERIMENTAL (in libnethogs) and may be removed or
    # changed in an incompatible way in a future release.
    # example:
    # FILTER = 'port 80 or port 8080 or port 443'
    FILTER = None

    #####################
    # END CONFIGURATION #
    #####################

    # You can use this to monitor only certain devices, like:
    # device_names = ['enp4s0', 'docker0']
    def __init__(self, 
                    pid_list, 
                    data_update_callback, 
                    device_names = None, 
                    start_monitoring=False, 
                    microsecond_update_interval: int = None,
                    debug=False):
        self._executing = False
        if (data_update_callback == None):
            BadUserInputError('data_update_callback is mandatory')

        self._data_update_callback = data_update_callback
        self.device_names = device_names
        
        self.pid_list = pid_list
        if microsecond_update_interval is None:
            microsecond_update_interval = 1000000
        self.__microsecond_update_interval = microsecond_update_interval
        self.__debug = debug

        libabspath = os.path.join(
                        os.path.dirname(os.path.abspath(__file__)),
                        NethogsUtil.LIBRARY_PATH,
                        NethogsUtil.LIBRARY_NAME)


        self.__lib = ctypes.CDLL(libabspath)
        if start_monitoring:
            self.start_monitoring()

    def start_monitoring(self):
        if not self._executing:
            self.monitor_thread = threading.Thread(
                target=self.__run_monitor_loop, args=()
            )
            self.monitor_thread.start()
            signal.signal(signal.SIGINT, self.__signal_handler)
            signal.signal(signal.SIGTERM, self.__signal_handler)
            

    def stop_monitoring(self):
        if self._executing:
            self.__lib.nethogsmonitor_breakloop()
      
    def __signal_handler(self, signal, frame):
        self.__lib.nethogsmonitor_breakloop()

    def __dev_args(self):
        """
        Return the appropriate ctypes arguments for a device name list, to pass
        to libnethogs ``nethogsmonitor_loop_devices``. The return value is a
        2-tuple of devc (``ctypes.c_int``) and devicenames (``ctypes.POINTER``)
        to an array of ``ctypes.c_char``).
        :param devnames: list of device names to monitor
        :type devnames: list
        :return: 2-tuple of devc, devicenames ctypes arguments
        :rtype: tuple
        """
        devc = len(self.device_names)
        devnames_type = ctypes.c_char_p * devc
        devnames_arg = devnames_type()
        for idx, val in enumerate(self.device_names):
            devnames_arg[idx] = (val + chr(0)).encode('ascii')
        return ctypes.c_int(devc), ctypes.cast(
            devnames_arg, ctypes.POINTER(ctypes.c_char_p)
        )

    def __pid_args(self):
        """
        Return the appropriate ctypes arguments for a pid list, to pass
        to libnethogs ``nethogsmonitor_loop_devices``. The return value is a
        2-tuple of devc (``ctypes.c_int``) and devicenames (``ctypes.POINTER``)
        to an array of ``ctypes.c_int``).
        :param devnames: list of device names to monitor
        :type devnames: list
        :return: 2-tuple of devc, devicenames ctypes arguments
        :rtype: tuple
        """
        pidc = len(self.pid_list)
        pid_list_type = ctypes.c_int * pidc
        pid_list_arg = pid_list_type()
        for idx, val in enumerate(self.pid_list):
            pid_list_arg[idx] = val
        return ctypes.c_int(pidc), ctypes.cast(
            pid_list_arg, ctypes.POINTER(ctypes.c_int)
        )


    def __run_monitor_loop(self):
        self._executing = True
        # Create a type for my callback func. The callback func returns void (None), and accepts as
        # params an int and a pointer to a NethogsMonitorRecord instance.
        # The params and return type of the callback function are mandated by nethogsmonitor_loop().
        # See libnethogs.h.
        CALLBACK_FUNC_TYPE = ctypes.CFUNCTYPE(
            ctypes.c_void_p, ctypes.c_int, ctypes.POINTER(NethogsMonitorRecord)
        )

        filter_arg = self.FILTER
        if filter_arg is not None:
            filter_arg = ctypes.c_char_p(filter_arg.encode('ascii'))

        
        if len(self.device_names) < 1:

            if len(self.pid_list) > 0:
                pidc, pidlist = self.__pid_args()
                # monitor all devices but sum pids
                rc = self.__lib.nethogsmonitor_loop_pids(
                    CALLBACK_FUNC_TYPE(self.__network_activity_callback), #cb
                    filter_arg, #filter
                    ctypes.c_int(0), #to_ms
                    pidc, #pidc
                    pidlist, #pid_list
                    ctypes.c_long(self.__microsecond_update_interval), #update_interval_us
                    ctypes.c_bool(self.__debug) #debug
                )
            else:
                # monitor all devices
                rc = self.__lib.nethogsmonitor_loop(
                    CALLBACK_FUNC_TYPE(self.__network_activity_callback),#cb
                    filter_arg,#filter
                    ctypes.c_int(0),#to_ms
                    ctypes.c_long(self.__microsecond_update_interval),#update_interval_us
                    ctypes.c_bool(self.__debug), #debug
                )
        else:
            devc, devicenames = self.__dev_args()
            if (len(self.pid_list) > 0):
                pidc, pidlist = self.__pid_args()
                rc = self.__lib.nethogsmonitor_loop_devices_pids(
                    CALLBACK_FUNC_TYPE(self.__network_activity_callback), #cb
                    filter_arg, #filter
                    devc, #devc
                    devicenames, #devicenames
                    ctypes.c_bool(False), #all
                    ctypes.c_int(0), #to_ms
                    pidc, #pidc
                    pidlist, #pid_list
                    ctypes.c_long(self.__microsecond_update_interval), #update_interval_us
                    ctypes.c_bool(self.__debug) #debug
                )
            else:
                rc = self.__lib.nethogsmonitor_loop_devices(
                    CALLBACK_FUNC_TYPE(self.__network_activity_callback), #cb
                    filter_arg, #filter
                    devc, #devc
                    devicenames, #devicenames
                    ctypes.c_bool(False), #all
                    ctypes.c_int(0), #to_ms
                    ctypes.c_long(self.__microsecond_update_interval), #update_interval_us
                    ctypes.c_bool(self.__debug)
                )

        if rc != LoopStatus.OK:
            print('nethogsmonitor_loop returned {}'.format(LoopStatus.MAP[rc]))
        else:
            self._executing = False
            print('exiting monitor loop')

    def __network_activity_callback(self, action, data):
        self._data_update_callback(data.contents.pid, 
                    data.contents.record_id,
                    data.contents.sent_bytes,
                    data.contents.sent_kbs,
                    data.contents.recv_bytes,
                    data.contents.recv_kbs)
