
from typing import Any, List, Tuple
import time
from process_monitor_lib.cpu_info import CpuInfo
from datetime import datetime
import os
from common_lib.common_csv import CommonCsv
from common_lib.common_base import TIMESTAMP_FORMAT
from common_lib.common_time_control import CommonTimeControl
from process_monitor_lib.monitor_type import MonitorType
import threading 

class ProcessInfo(CommonCsv, CommonTimeControl):

    # __LOG_HEAD = ['timestamp', 'total_time_microseconds', 'delta_time_microseconds', 'sent_bytes', 'sent_bps', 
    __LOG_HEAD = ['total_time_microseconds', 'delta_time_microseconds', 'sent_bytes', 'sent_bps', 
                    'recv_bytes', 'recv_bps', 'cpu_per', 'cpu_per_avg', 'men_bytes', 'men_per']
    

    __lock_update_statistics = threading.Lock()

    def __init__(self,
                pid: int,
                log_directory: str,
                microsecond_update_interval: int = None,
                monitor_type: MonitorType = MonitorType.BOTH,
                debug=False):
        self.__process = CpuInfo(pid)
        CommonCsv.__init__(self=self, 
                            csv_header= self.__LOG_HEAD, 
                            log_directory= log_directory, 
                            log_file_name= self.get_log_file_name(log_directory, 
                                                                    self.__process.name(), 
                                                                    self.__process.pid))
        CommonTimeControl.__init__(self=self, auto_start=False)
        self.__meansurements =[]
        self.debug = debug
        self.sent_bytes = 0
        self.recv_bytes = 0
        self.sent_kbs = 0.0
        self.recv_kbs = 0.0
        self.cpu_percentage = 0.0
        self.cpu_percentage_average = 0.0
        self.ram_memory_bytes = 0
        self.ram_memory_percentage = 0.0
        self.__record_id = 0
        self.__accumulated_sent_bytes = 0
        self.__accumulated_recv_bytes = 0
        #self.__second_update_interval = (1.0 / 1000000.0) * microsecond_update_interval
        self.__microsecond_update_interval = microsecond_update_interval
        self.__running = True
        self.__process.cpu_percent()
        #self.__update_process_info(cpu_refresh_interval_seconds=self.__second_update_interval)
        self.__update_process_info(cpu_refresh_interval_microseconds=self.__microsecond_update_interval)
        self._last_process_info = (self.cpu_percentage, self.ram_memory_bytes, self.ram_memory_percentage)
        # self.update_statistic()
        self.__thead_log = threading.Thread(target=self.__prepare_log_to_write)
        self.__thead_log.start()
        if monitor_type == MonitorType.CPU or monitor_type == MonitorType.BOTH: 
            self.__thead_publisher = threading.Thread(target=self.schedure_repetition)
            self.__thead_publisher.daemon = True
            self.__thead_publisher.start()


    def on_stop(self):
        num_rows_to_prepare = 0
        while True:
            with self.__lock_update_statistics:
                num_rows_to_prepare = len(self.__meansurements)
            if num_rows_to_prepare == 0:
                break
            print("waiting " + str(num_rows_to_prepare) + " logs to be prepared to be written")
            time.sleep(1)
        
        self.stop_csv_writting()
        self.__running = False
        


    def schedure_repetition(self):
        while self.__running:
            self.update_statistic()

    def update_statistic(self):
        log_to_write = None
        #self.__update_process_info(cpu_refresh_interval_microseconds=self.__second_update_interval)
        self.__update_process_info(cpu_refresh_interval_microseconds=self.__microsecond_update_interval)
        process_info_tuple = (self.cpu_percentage, self.ram_memory_bytes, self.ram_memory_percentage)
        if self._last_process_info != process_info_tuple:
            self._last_process_info = process_info_tuple
            self.__add_meansurement(sent_bytes = self.sent_bytes,
                                    sent_kbs = self.sent_kbs,
                                    recv_bytes =self.recv_bytes,
                                    recv_kbs = self.recv_kbs,
                                    cpu_percentage = self.cpu_percentage,
                                    cpu_percentage_average = self.cpu_percentage_average,
                                    ram_memory_bytes = self.ram_memory_bytes,
                                    ram_memory_percentage = self.ram_memory_percentage)

    def get_process_name(self):
        return self.__process.name()


    @staticmethod
    def get_log_file_name(log_directory, process_name, pid):
        path_separator = ''
        if (not log_directory.endswith(os.path.sep)):
            path_separator = os.path.sep

        return "{0}{1}{2}_pid_{3}_{4}.csv".format(log_directory, 
                                path_separator,
                                process_name,
                                pid,
                                datetime.now().strftime('%Y%m%d%H%M%S'))
    
    def update_data(self, record_id, sent_bytes: int, recv_bytes: int, sent_kbs, recv_kbs):
            self.__update_process_info()
            if (self.__record_id != record_id):
                self.__record_id  = record_id
                self.__accumulated_sent_bytes = self.sent_bytes
                self.__accumulated_recv_bytes = self.recv_bytes
            
            self.sent_bytes = self.__accumulated_sent_bytes + sent_bytes
            self.recv_bytes = self.__accumulated_recv_bytes + recv_bytes
            self.sent_kbs = sent_kbs
            self.recv_kbs = recv_kbs

            self.__add_meansurement(sent_bytes = self.sent_bytes,
                                        sent_kbs = self.sent_kbs,
                                        recv_bytes =self.recv_bytes,
                                        recv_kbs = self.recv_kbs,
                                        cpu_percentage = self.cpu_percentage,
                                        cpu_percentage_average = self.cpu_percentage_average,
                                        ram_memory_bytes = self.ram_memory_bytes,
                                        ram_memory_percentage = self.ram_memory_percentage)

    def __update_process_info(self, cpu_refresh_interval_microseconds: float=None):
        self.cpu_percentage = self.__process.cpu_percent(microseconds_interval=cpu_refresh_interval_microseconds)
        self.cpu_percentage_average = (self.cpu_percentage + self.cpu_percentage_average) / 2
        self.ram_memory_bytes = self.__process.memory_info().vms
        self.ram_memory_percentage = self.__process.memory_percent()

    def __print_process_statistics(self):
        print("CPU = " + str(self.cpu_percentage) + "% | " +
               "Memory: {0:d} Bytes, {1:.2f}% |".format(self.ram_memory_bytes, self.ram_memory_percentage) +
               "Tx: " + str(self.sent_bytes) + " Bytes, Taxa: " + "{0:.2f}".format(self.sent_kbs) + "KB/s | " + 
               "Rx:" + str(self.recv_bytes) + " Bytes, Taxa: " + "{0:.2f}".format(self.recv_kbs) + "KB/s", end="\r")

    def __add_meansurement(self,
                            sent_bytes,
                            sent_kbs,
                            recv_bytes,
                            recv_kbs,
                            cpu_percentage,
                            cpu_percentage_average,
                            ram_memory_bytes,
                            ram_memory_percentage) -> None:
        with self.__lock_update_statistics:
            self._update_time()
            self.__meansurements.append((self.get_total_time_microseconds(),
                                            self.get_delta_time_microseconds(),
                                            sent_bytes,
                                            sent_kbs,
                                            recv_bytes,
                                            recv_kbs,
                                            cpu_percentage,
                                            cpu_percentage_average,
                                            ram_memory_bytes,
                                            ram_memory_percentage))

    def __prepare_log_to_write(self) -> None:
        while self.__running:
            num_rows_to_prepare = len(self.__meansurements)
            if num_rows_to_prepare > 0:
                for meansurement in self.__meansurements[0:num_rows_to_prepare]:
                    log_row = self.__get_log_to_write(meansurement_tuple=meansurement)
                    self.append_log(log_row=log_row)
                    if (self.debug): 
                        self.__print_process_statistics()
                with self.__lock_update_statistics:
                    del self.__meansurements[0:num_rows_to_prepare]
            time.sleep(1)


    def __get_log_to_write(self, meansurement_tuple) -> List[Any]:
        (total_time_microseconds,
        delta_time_microseconds,
        sent_bytes,
        sent_kbs,
        recv_bytes,
        recv_kbs,
        cpu_percentage,
        cpu_percentage_average,
        ram_memory_bytes,
        ram_memory_percentage) = meansurement_tuple

        log_to_write = []
        log_to_write.append(total_time_microseconds)
        log_to_write.append(delta_time_microseconds)
        log_to_write.append(sent_bytes)
        log_to_write.append("{0:.2f}".format(sent_kbs*1024.0))
        log_to_write.append(recv_bytes)
        log_to_write.append("{0:.2f}".format(recv_kbs*1024.0))
        log_to_write.append("{0:.2f}%".format(cpu_percentage))
        log_to_write.append("{0:.2f}%".format(cpu_percentage_average))
        log_to_write.append(ram_memory_bytes)
        log_to_write.append("{0:.2f}%".format(ram_memory_percentage))
        return log_to_write


    def get_tx_bytes(self):
        return self.sent_bytes

    def get_rx_Bytes(self):
        return self.recv_bytes