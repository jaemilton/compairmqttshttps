#!/usr/bin/python3
# -*- coding: UTF-8 -*-x
from typing import Dict, List
import psutil
import subprocess
import threading
import os
import signal
import time
import sys
import datetime


sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)));
from process_monitor_lib.nethogs_util import NethogsUtil
from process_monitor_lib.process_info import ProcessInfo
from process_monitor_lib.monitor_type import MonitorType

class ProcessMonitor(NethogsUtil):

    def __init__(self, 
                process_id_list, 
                log_directory, 
                device_names=[], 
                microsecond_update_interval: int = None,
                monitor_type: MonitorType = MonitorType.BOTH,
                monitor_just_children: bool = True,
                debug: bool=False) -> None:
        print("Monitoring type ==> " + str(monitor_type)) 
        start_network_monitoring = monitor_type is MonitorType.NETWORK or monitor_type is MonitorType.BOTH
        print("Start network monitoging ==> " + str(start_network_monitoring))   
        NethogsUtil.__init__(self=self, 
                            pid_list=process_id_list, 
                            data_update_callback=self.__update_data_callback, 
                            device_names=device_names,
                            microsecond_update_interval=microsecond_update_interval,
                            start_monitoring=start_network_monitoring,
                            debug=debug)
        self._process_id_list = []
        for pid in process_id_list:
            process = psutil.Process(pid)
            if monitor_just_children:
                child_processes:List[psutil.Process] = process.children()
                if child_processes:
                    print("pid " + str(process.pid) + " has " + str(len(child_processes)) + " child(s) process(es). just the child processes will be monitored"  )
                    for child_process in child_processes:
                        self.add_pid(child_process.pid)
                else:
                    self.add_pid(process.pid)
            else:
                self.add_pid(process.pid)

        self._dictionary_process_info = {}
        
        for process_id in self._process_id_list:
            self._dictionary_process_info[process_id] = \
                ProcessInfo(pid = process_id, 
                            log_directory=log_directory, 
                            microsecond_update_interval=microsecond_update_interval,
                            monitor_type=monitor_type,
                            debug=debug)
            print("Monitoring pid " + str(process_id))
        print("Update statistics interval " + str(microsecond_update_interval) + " microseconds." )

    def add_pid(self, pid: int):
        if not pid in self._process_id_list:
            self._process_id_list.append(pid)

    def on_stop(self):
        self.stop_monitoring()
        for process_id in self._process_id_list:
            self._dictionary_process_info[process_id].on_stop()
        
  
    def __update_data_callback(self,
                        pid,
                        record_id, 
                        sent_bytes, 
                        sent_kbs, 
                        recv_bytes,
                        recv_kbs):
        
        if pid in self._dictionary_process_info :
            process_info = self._dictionary_process_info[pid]
            process_info.update_data(record_id,
                sent_bytes,
                recv_bytes,
                sent_kbs,
                recv_kbs)


    @staticmethod
    def check_if_process_is_running(process_name):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if process_name.lower() in proc.name().lower():
                    return True
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return False

    @staticmethod
    def get_first_pid_by_name(process_name):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if process_name.lower() in proc.name().lower():
                    return proc.pid
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return 0
    
    @staticmethod
    def get_pid_list_by_name(process_name):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        pidList = []
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if proc.name().lower().endswith(process_name.lower()):
                    pidList.append(proc.pid)
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return pidList


    def get_processes_name(self):
        processes_name = ""
        for process_id in self._process_id_list:
            processes_name+= self._dictionary_process_info[process_id].get_process_name() + ","
        return processes_name

