#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from result_consolidation_lib.consolidation_type import ConsolidationType
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters
from common_lib.common_error import BadUserInputError
from result_consolidation_lib.server_consolidation import ServerConsolidation
from result_consolidation_lib.client_consolidation import ClientConsolidation
import sys

class ResultConsolitation(object):
    def __init__(self, test_directory_path: str, 
                        consolidation_type: ConsolidationType,
                        server_consolidation_column_name:str,
                        client_consolidation_column_name:str
                        ) -> None:

        self.server_consolidation = None
        self.client_consolidation = None
        if consolidation_type == ConsolidationType.BOTH:
            self.server_consolidation = ServerConsolidation(test_directory_path=test_directory_path, 
                                                            server_consolidation_column_name=server_consolidation_column_name)
            self.client_consolidation = ClientConsolidation(test_directory_path=test_directory_path, 
                                                            client_consolidation_column_name=client_consolidation_column_name)
        elif consolidation_type == ConsolidationType.CLIENT:
            self.client_consolidation = ClientConsolidation(test_directory_path=test_directory_path,
                                                            client_consolidation_column_name=client_consolidation_column_name)
        elif consolidation_type == ConsolidationType.SERVER:
            self.server_consolidation = ServerConsolidation(test_directory_path=test_directory_path,
                                                            server_consolidation_column_name=server_consolidation_column_name)
        else:
            raise BadUserInputError("The consolidation type must be set")
            

    def consolidate(self) -> None:
        if self.server_consolidation:
            self.server_consolidation.consolidate()
        if self.client_consolidation:
            self.client_consolidation.consolidate()

def start(argv):
    if '-?' in argv:
        print("""Application to consolidate result between https websocket and mqtts server file results
        Usage: python3 result_consolidation.py -d TEST_RESULT_PATH [-t CONSOLIDATION TYPE] [-c "SERVER_COLUMN_NAME|CLIENT_COLUMN_NAME"] [-?] [-v] 

                -d : Testresults path to be consolidates
                -t : Consolidation type, optional, can be SERVER or CLIENT or BOTH, if not set, will assume BOTH
                -c : Consolidation column name, optional, "SERVER_COLUMN_NAME,CLIENT_COLUMN_NAME" if not set, will assume "total_time_microseconds,total_microseconds"
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    elif valid_mandatory_parameters(argv, ['-d']):
        raise BadUserInputError(
            "Input error. To run, call as python3 result_consolidation.py -d TEST_RESULT_PATH [-t CONSOLIDATION TYPE] [-c \"SERVER_COLUMN_NAME,CLIENT_COLUMN_NAME\"] [-?] [-v]")
    else:
        test_directory_path = get_input_parameter_value(argv, '-d')
        
        consolidation_type_param = get_input_parameter_value(argv, '-t')
        if not consolidation_type_param:
            consolidation_type = ConsolidationType.BOTH
        else:
            consolidation_type= ConsolidationType[consolidation_type_param.upper()]

        consolidation_column_names_param:str = get_input_parameter_value(argv, '-c')
        (server_consolidation_column_name, client_consolidation_column_name) = \
                get_consolidation_column_names_param(consolidation_type=consolidation_type,  
                                                    param=consolidation_column_names_param)

        result_consolidation = ResultConsolitation(test_directory_path=test_directory_path, 
                                                    consolidation_type=consolidation_type,
                                                    server_consolidation_column_name=server_consolidation_column_name,
                                                    client_consolidation_column_name=client_consolidation_column_name)
        result_consolidation.consolidate()

def get_consolidation_column_names_param(consolidation_type:ConsolidationType, param:str):
    server_consolidation_column_name = "total_time_microseconds"
    client_consolidation_column_name = "total_microseconds"
    if param:
        if consolidation_type == ConsolidationType.BOTH:
            consolidation_column_names = param.split(";")
            if len(consolidation_column_names) == 2:
                server_consolidation_column_name = consolidation_column_names[0]
                client_consolidation_column_name = consolidation_column_names[1]
            else:
                raise BadUserInputError("The consolidation column name must be defined for both, server and client respectivly.")
        elif consolidation_type == ConsolidationType.CLIENT:
            client_consolidation_column_name = param
        elif consolidation_type == ConsolidationType.SERVER:
            server_consolidation_column_name = param

    return (server_consolidation_column_name, client_consolidation_column_name)


if __name__ == '__main__':
    start(sys.argv)
