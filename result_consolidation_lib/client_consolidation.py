#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from result_consolidation_lib.consolidation_type import ConsolidationType
from result_consolidation_lib.consolidation_base import ConsolidationBase

class ClientConsolidation(ConsolidationBase):

    def __init__(self, test_directory_path: str, client_consolidation_column_name:str) -> None:
        ConsolidationBase.__init__(self=self, 
                                    test_directory_path=test_directory_path, 
                                    consolidation_type=ConsolidationType.CLIENT,
                                    consolidation_columns=["accumulated_delta_microseconds",
                                                            "total_microseconds",
                                                            "counter", 
                                                            "microseconds_reveived_message", 
                                                            "microseconds_reveived_response", 
                                                            "delta_microseconds"
                                                            ],
                                    consolidation_by_column_name=client_consolidation_column_name)
       
