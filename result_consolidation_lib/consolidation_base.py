#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
from result_consolidation_lib.consolidation_generator import ConsolidationGenerator
from typing import List
from common_lib.common_error import BadUserInputError
from result_consolidation_lib.consolidation_type import ConsolidationType


class ConsolidationBase(object):

    CONSOLIDATED_FILE_NAME_FORMAT = "{consolidation_type}_consolidation_{consolidated_by}{datestr}_{log_directory_number:0>2}.csv"

    def __init__(self, 
                test_directory_path: str,
                consolidation_type: ConsolidationType,
                consolidation_columns: List[str] = None,
                consolidation_by_column_name: str = None) -> None:
        self.consolidation_type = consolidation_type
        self.consolidation_output_columns = consolidation_columns
        self.consolidation_columns_indexs = None if consolidation_columns is None else []
        side_folder_name = "server_side" if consolidation_type == ConsolidationType.SERVER else "client_side"
        self.consolidation_by_column_name = consolidation_by_column_name
        self.csv_consolidated_header = []
        self.csv_common_header = []
        self.consolidated_rows = []
        self.consolidated_by = "" if self.consolidation_by_column_name is None else "by_" + self.consolidation_by_column_name + "_"

        if self.consolidation_output_columns and self.consolidation_by_column_name:
            if not self.consolidation_by_column_name in self.consolidation_output_columns:
                raise BadUserInputError("The column " + self.consolidation_by_column_name + " must be on output columns")

        self.adjust_output_column_order()

        self.test_directory_path = test_directory_path

        if not os.path.exists(self.test_directory_path):
            raise BadUserInputError("There is no Testresult path " + self.test_directory_path)
        self.test_directory_name = os.path.basename(self.test_directory_path)

        self.https_websocket_path = os.path.join(self.test_directory_path, "https_websocket", side_folder_name)
        self.mqtts_path = os.path.join(self.test_directory_path, "mqtts", side_folder_name)

        if not os.path.exists(self.https_websocket_path):
            raise BadUserInputError("There is  https_websocket/server_side no Testresult path " + self.https_websocket_path)
        
        if not os.path.exists(self.mqtts_path):
            raise BadUserInputError("There is no mqtts/server_side path " + self.mqtts_path)

        self.https_websocket_files = os.listdir(self.https_websocket_path)
        self.mqtts_files = os.listdir(self.mqtts_path)

        #sort the file names
        self.https_websocket_files.sort()
        self.mqtts_files.sort()

        #ensure that the number of files are the same between https_websocket and mqtts
        if len(self.https_websocket_files) != len(self.mqtts_files):
            raise BadUserInputError("There are " +  str(len(self.https_websocket_files)) + 
                                    " files on https_websocket side and " +
                                    str(len(self.mqtts_files)) + " on mqtts side.")
        
        self.datestr = self.test_directory_name[5:13]

  
    def adjust_output_column_order(self):
        #adjust the order for consolidation_column_name be the first
        if self.consolidation_output_columns and self.consolidation_output_columns.index(self.consolidation_by_column_name) > 0:
            self.consolidation_output_columns.remove(self.consolidation_by_column_name)
            self.consolidation_output_columns.insert(0, self.consolidation_by_column_name)

    def consolidate(self) -> None:
        
        index = 0
        for https_websocket_file in self.https_websocket_files:
            consolidate_file_name = self.CONSOLIDATED_FILE_NAME_FORMAT.format(consolidation_type=self.consolidation_type.name.lower(), 
                                                                                datestr=self.datestr, 
                                                                                consolidated_by=self.consolidated_by,
                                                                                log_directory_number=index)

            consolidate_file_path = os.path.join(self.test_directory_path, consolidate_file_name)
            mqtts_file = self.mqtts_files[index]

            https_websocket_file_path =  os.path.join(self.https_websocket_path, https_websocket_file)
            mqtts_file_path = os.path.join(self.mqtts_path, mqtts_file)

            consolidation_generator = ConsolidationGenerator(consolidate_file_path=consolidate_file_path,
                                                                https_websocket_file_path=https_websocket_file_path,
                                                                mqtts_file_path=mqtts_file_path,
                                                                consolidation_columns=self.consolidation_output_columns,
                                                                consolitation_by_column_name=self.consolidation_by_column_name)

            consolidation_generator.consolidate_files()
            index +=1
        

   