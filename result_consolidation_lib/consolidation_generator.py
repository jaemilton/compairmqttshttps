#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
import csv
from typing import Any, List, Tuple
from common_lib.common_base import array_are_equal, parse_float
from common_lib.common_error import BadUserInputError

class ConsolidationGenerator(object):


    def __init__(self, 
                consolidate_file_path: str, 
                https_websocket_file_path: str, 
                mqtts_file_path: str,
                consolidation_columns: List[str] = None,
                consolitation_by_column_name: str = None) -> None:
      

         #if the consolidated file alread exists, remove it to generate a new one
        if  os.path.exists(consolidate_file_path):
            os.remove(consolidate_file_path)
        
        if not os.path.exists(https_websocket_file_path):
            raise BadUserInputError("There is  https_websocket/server_side no Testresult path " + self.https_websocket_path)

        if not os.path.exists(mqtts_file_path):
            raise BadUserInputError("There is no mqtts/server_side path " + self.mqtts_path)

        self.consolidate_file_path = consolidate_file_path
        self.https_websocket_file_path = https_websocket_file_path
        self.mqtts_file_path = mqtts_file_path


        self.consolidation_columns = consolidation_columns
        self.consolidation_columns_index = None if consolidation_columns is None else []
        self.consolitation_by_column_name = consolitation_by_column_name
        self.csv_consolidated_header = []
        self.csv_common_header = []
        self.consolidated_rows = []

    def consolidate_files(self)-> None:
        https_websocket_rows = []
        mqtts_rows = []

        #read all lines of https_websocket_server_file to memory
        with open(self.https_websocket_file_path) as https_websocket_csv_file:
            https_websocket_server_file_reader = csv.reader(https_websocket_csv_file, delimiter=';')
            for https_websocket_row in https_websocket_server_file_reader:
                https_websocket_rows.append(https_websocket_row)

        #read all lines of mqtt_server_file to memory
        with open(self.mqtts_file_path) as mqtts_csv_file:
            mqtt_file_reader = csv.reader(mqtts_csv_file, delimiter=';')
            for mqtts_row in mqtt_file_reader:
                mqtts_rows.append(mqtts_row)

        #ensure thar there are lines to be read
        if len(https_websocket_rows) <= 1 or len(mqtts_rows) <= 1:
            raise BadUserInputError("Both files must have rows do be consolidated.")

        #endure tha heades of both files are the same
        if not array_are_equal(arr1 = https_websocket_rows[0], arr2=mqtts_rows[0]):
            raise BadUserInputError("Both files must have the same header")

        self.get_common_header(header_row=https_websocket_rows[0])

        if self.consolitation_by_column_name is not None and not self.consolitation_by_column_name in self.csv_common_header:
            raise BadUserInputError("There is no consolitation_by_column_name " + self.consolitation_by_column_name  + " on csv header")

        self.generate_consolidated_header()
        
        
        self.create_consolidated_rows(https_websocket_rows, mqtts_rows)

        with open(file=self.consolidate_file_path, mode='w', newline='') as csv_consolidate_file:
            csv_consolidate_writer = csv.writer(csv_consolidate_file, 
                                            delimiter=';', quotechar='"', 
                                            quoting=csv.QUOTE_MINIMAL)
            #write the Header
            csv_consolidate_writer.writerow(self.csv_consolidated_header)                      
            #write the lines
            csv_consolidate_writer.writerows(self.consolidated_rows)

    def get_common_header(self, header_row: List[str]):
        for column_name in header_row:
            self.csv_common_header.append(column_name)

    def generate_consolidated_header(self):
        if not self.consolitation_by_column_name:
            self.csv_consolidated_header.append("row_number")

        if self.consolidation_columns:
            for consolidation_colunm_name in self.consolidation_columns:
                index_column = self.csv_common_header.index(consolidation_colunm_name)
                self.add_csv_consolidated_header_column(consolidation_colunm_name)
                self.consolidation_columns_index.append(index_column)
        else:
            for column_name in self.csv_common_header:
                self.add_csv_consolidated_header_column(column_name)

    def add_csv_consolidated_header_column(self, column_name):
        if column_name == self.consolitation_by_column_name:
            self.csv_consolidated_header.append(column_name)
        else:
            self.csv_consolidated_header.append("wss_" + column_name)
            self.csv_consolidated_header.append("mqtts_" + column_name)


    def create_consolidated_rows(self, https_websocket_rows: List[Any], mqtts_rows: List[Any]):
        
        https_websocket_line_count = 1
        mqtts_csv_line_count = 1
        mqtts_csv_line_number = len(mqtts_rows)
        #read all rows of https_websocket_server_rows
        for https_websocket_row in https_websocket_rows[https_websocket_line_count:]:
            https_row_written = False
            while not https_row_written:
                mqtts_row = None if mqtts_csv_line_count == mqtts_csv_line_number else mqtts_rows[mqtts_csv_line_count]
                (https_websocket_row_to_write, mqtts_row_to_write) = self.get_rows_to_be_written(mqtts_row, https_websocket_row)
                self.add_consolidated_row(row=self.create_consolidated_row(https_websocket_row=https_websocket_row_to_write, 
                                                                    mqtts_row=mqtts_row_to_write))
                
                if mqtts_csv_line_count < mqtts_csv_line_number and mqtts_row_to_write is not None:
                    mqtts_csv_line_count += 1
                https_row_written = (https_websocket_row_to_write is not None)

        #read all remaining rows of mqtts_server_rows
        for mqtts_row in mqtts_rows[mqtts_csv_line_count:]:
            self.add_consolidated_row(row=self.create_consolidated_row(mqtts_row=mqtts_row))
            mqtts_csv_line_count += 1
            

    def add_consolidated_row(self, row:List[Any]) -> None:
        if not self.consolitation_by_column_name:
            csv_consolidated_row_count = len(self.consolidated_rows)+1
            row.insert(0, csv_consolidated_row_count)
        self.consolidated_rows.append(row)

    def get_attrubute_values(self, mqtts_row: List[Any], https_websocket_row:List[Any], column_index) -> Tuple[Any, Any]:
        mqtt_value = None
        https_websocket_value = None

        if column_index >= 0:
            if mqtts_row is not None:
                mqtt_value = self.get_get_attrubute_value(mqtts_row[column_index])

            if https_websocket_row is not None:
                https_websocket_value = self.get_get_attrubute_value(https_websocket_row[column_index])
        
        return (mqtt_value, https_websocket_value) 

    def get_get_attrubute_value(self, value) -> Any:
        return_value = None
        if value is not None:
            float_value = parse_float(value)
            return_value = value if float_value is None else float_value
        return return_value



    def get_rows_to_be_written(self, mqtts_row: List[Any], https_websocket_row: List[Any]) -> Tuple[List[Any], List[Any]]:
        https_websocket_row_to_write = None
        mqtts_row_to_write = None
        common_column_index = self.get_common_column_index()

        if common_column_index >= 0: #check of the consolidation colun was defined
            (mqtt_value, https_websocket_value) = self.get_attrubute_values(mqtts_row=mqtts_row, 
                                                                            https_websocket_row=https_websocket_row, 
                                                                            column_index=common_column_index)

            #check if there is no mqtts_row or if https_websocket common column is shotter than mqtts
            if  mqtt_value is None or https_websocket_value < mqtt_value:
                https_websocket_row_to_write = https_websocket_row
                mqtts_row_to_write = None
            #check if there is no https_websocket_row or if https_websocket common column is gratter than mqtts
            elif https_websocket_value is None or https_websocket_value > mqtt_value:
                https_websocket_row_to_write = None
                mqtts_row_to_write = mqtts_row
            #else when the common column ar equal between https_websocket and mqtts
            else:
                https_websocket_row_to_write = https_websocket_row
                mqtts_row_to_write = mqtts_row
        else:
            https_websocket_row_to_write = https_websocket_row
            mqtts_row_to_write = mqtts_row


        return (https_websocket_row_to_write, mqtts_row_to_write)

    def create_consolidated_row(self, https_websocket_row: List[Any] = None, mqtts_row: List[Any] = None) -> List[Any]:
        columns = (self.consolidation_columns 
                    if self.consolidation_columns is not None 
                    else self.csv_common_header)

        consolidation_column_value = None
        common_column_index = self.get_common_column_index()
        if common_column_index >= 0: #check of the consolidation colun was defined
            consolidation_column_value = (mqtts_row[common_column_index] 
                                            if https_websocket_row is None 
                                            else https_websocket_row[common_column_index])
        consolidate_row = []
        
        for column_name in columns:
            column_index = self.csv_common_header.index(column_name)
            if consolidation_column_value and column_index == common_column_index:
                consolidate_row.append(consolidation_column_value)
            else:
                consolidate_row.append(None if https_websocket_row is None else https_websocket_row[column_index])
                consolidate_row.append(None if mqtts_row is None else mqtts_row[column_index])
        return consolidate_row
        
    def get_common_column_index(self) -> int:
        return -1 if self.consolitation_by_column_name is None else self.csv_common_header.index(self.consolitation_by_column_name)