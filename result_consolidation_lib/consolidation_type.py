#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from enum import Enum

class ConsolidationType(Enum):
     SERVER = 1
     CLIENT = 2
     BOTH = 3