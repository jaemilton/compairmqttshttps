#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from result_consolidation_lib.consolidation_base import ConsolidationBase
from result_consolidation_lib.consolidation_type import ConsolidationType

class ServerConsolidation(ConsolidationBase):
    
    def __init__(self, test_directory_path: str, server_consolidation_column_name:str) -> None:
        ConsolidationBase.__init__(self=self, 
                                    test_directory_path=test_directory_path, 
                                    consolidation_type=ConsolidationType.SERVER,
                                    consolidation_columns=None,
                                    consolidation_by_column_name=server_consolidation_column_name)

