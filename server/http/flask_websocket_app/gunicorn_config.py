#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import multiprocessing

bind = '0.0.0.0:443'
worker_class = 'gevent'
max_requests = 0
worker_connections = 100000
workers = 1
threads = multiprocessing.cpu_count() * 4
#reload = True
ca_certs = '/etc/ssl/private/mqtt-ca.crt'
certfile = '/etc/ssl/certs/mqtt-server.crt'
keyfile = '/etc/ssl/private/mqtt-server.key'

#logging
accesslog = '-'
errorlog = '-'