#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys
import psutil
from flask import Flask
from flask_socketio import SocketIO
from engineio.payload import Payload
debug = False

class WebSocketServer(Flask):
    
    def __init__(self):
        Flask.__init__(self, import_name=__name__)
        Payload.max_decode_packets = 10000000
        self.received_messages = 0
        #To avoid problems of client disconnection, was necessary to increase the 
        # ping timeout from default to 120 seconds 
        # ping interval from default to 60 seconds
        # max_http_buffer_size from default to 2147483648 bytes (2GBytes)
        self.socketio = SocketIO(self, 
                                async_mode="gevent", 
                                ping_timeout=120, 
                                ping_interval=60,
                                 max_http_buffer_size=2147483648, 
                                 manage_session=False,
                                 cors_credentials=False,
                                 http_compression=False,
                                 cookie=None)
        self.socketio.on_event('event', self.on_message)
        self.socketio.on_event('connect', self.on_connect)
        self.socketio.on_event('disconnect', self.on_disconnect)
        #self.socketio.on(self.on_namespace)
        self.add_url_rule('/health', 'health', self.index)
        

    def start(self):
        if debug:
            print ("Server PID ==> " + str(psutil.Process().pid))
            self.socketio.run(self, host="0.0.0.0", debug=False, use_reloader=False)
        else:
            self.socketio.run(self)

    #@app.route('/health')
    def index(self):
        return "OK" 
    
    def on_message(self, topic_name, message):
        """
        
        """
        self.socketio.emit(topic_name, message, broadcast=True, include_self=False)

    def on_connect(self):
        print("Client connected")

    def on_disconnect(self):
        print("Client disconnected.")

    def on_namespace(self, namespace):
        print("Namespace created ==>" + namespace)

app = WebSocketServer()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        debug = (sys.argv[1] == "-v")
    app.start()

