#!/bin/bash

echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path


EUID="id -u"
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "#### Installing  python3, python3-pip and python-dev"
apt-get install libssl-dev python3 python3-pip python-dev -y
python3 -m pip install --upgrade pip
echo ""

echo "#### Installing python3 library Flask, flask_socketio, gunicorn"
pip3 install Flask==1.1.2
pip3 install Flask-SocketIO==5.0.1
#pip3 install eventlet
pip3 install gevent==20.12.1
pip3 install gevent-websocket==0.10.1
pip3 install gunicorn==20.0.4
#pip install gunicorn[gevent]
#pip3 install uvicorn[standard]
#pip3 install fastapi
echo ""

echo "#### Publishing the application Flask Websocked"
rm -rf /opt/app/flask_websocket_app
echo ""

SERVICE=/etc/systemd/system/flask-websocket-ssl.service
if [ -f "$SERVICE" ]; then
  echo "#### Removing existent service flask-websocket-ssl.service"
  systemctl stop flask-websocket-ssl.service
  systemctl disable flask-websocket-ssl.service
  rm -f /etc/systemd/system/flask-websocket-ssl.service
  systemctl daemon-reload
  echo ""
fi

mkdir -p /opt/app/
cp -rf $dir_path/flask_websocket_app /opt/app/
chown -R www-data:www-data /opt/app/flask_websocket_app


echo "#### configuring ssl website"
cp -f $dir_path/../certs/mqtt-server.crt /etc/ssl/certs/mqtt-server.crt
cp -f $dir_path/../certs/mqtt-server.key /etc/ssl/private/mqtt-server.key
cp -f $dir_path/../../certs/mqtt-ca.crt /etc/ssl/private/mqtt-ca.crt

echo "#### Configuring service flask-websocket-ssl.service"
cp -f $dir_path/flask-websocket-ssl.service /etc/systemd/system/flask-websocket-ssl.service
systemctl daemon-reload
systemctl start flask-websocket-ssl.service
echo ""
