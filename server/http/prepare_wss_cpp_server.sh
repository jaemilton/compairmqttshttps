#!/bin/bash

echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path


EUID="id -u"
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "#### Installing cmake, git"
apt install gcc-8 libssl-dev libboost-system-dev libboost-filesystem-dev libboost-thread-dev libboost-context-dev libboost-coroutine-dev cmake git -y
echo ""

echo "#### Compilling websocket server"
git clone https://gitlab.com/jaemilton/Simple-WebSocket-Server.git
cd Simple-WebSocket-Server 
mkdir build
cd build
cmake ..
#make
cpu_count=$(python3 -c 'import os; print(str(os.cpu_count() + 2))')
cmake --build $dir_path/Simple-WebSocket-Server/build --config Release --target wss_broadcast_server -j $cpu_count
echo ""



echo "#### Publishing the application C++ websocket_broadcast_app"
rm -rf /opt/app/websocket_broadcast_app
echo ""

SERVICE=/etc/systemd/system/cpp-websocket-ssl.service
if [ -f "$SERVICE" ]; then
  echo "#### Removing existent service cpp-websocket-ssl.service"
  systemctl stop cpp-websocket-ssl.service
  systemctl disable cpp-websocket-ssl.service
  rm -f /etc/systemd/system/cpp-websocket-ssl.service
  systemctl daemon-reload
  echo ""
fi

mkdir -p /opt/app/cpp_websocket_broadcast_app
cp $dir_path/Simple-WebSocket-Server/build/wss_broadcast_server /opt/app/cpp_websocket_broadcast_app


chown -R www-data:www-data /opt/app/cpp_websocket_broadcast_app

echo "#### removing cpp_websocket_broadcast_app source code "
cd $dir_path
rm -rf ./Simple-WebSocket-Server
echo ""


echo "#### configuring ssl websocket"
cp -f $dir_path/../certs/mqtt-server.crt /opt/app/cpp_websocket_broadcast_app/mqtt-server.crt
cp -f $dir_path/../certs/mqtt-server.key /opt/app/cpp_websocket_broadcast_app/mqtt-server.key
cp -f $dir_path/../../certs/mqtt-ca.crt /opt/app/cpp_websocket_broadcast_app/mqtt-ca.crt
echo ""

echo "#### Configuring service cpp-websocket-ssl.service"
cp -f $dir_path/cpp-websocket-ssl.service /etc/systemd/system/cpp-websocket-ssl.service
systemctl daemon-reload
systemctl start cpp-websocket-ssl.service
echo ""
