#!/bin/bash

echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path


IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi

echo "#### Installing  mosquitto"
apt-get install mosquitto -y
echo ""

echo "#### Stopping mosquitto Service"
systemctl stop mosquitto.service
echo ""

echo "#### Installing  python3, python3-pip, python3-wheel, gcc, python3-dev mosquitto, libpcap0.8"
apt-get install python3 python3-pip -y
apt-get install python3-wheel -y
apt-get install gcc -y
apt-get install libpcap0.8 -y
PACKAGE_NAME=$(python3 -c 'import sys; print("python" + str(sys.version_info.major) + "." + str(sys.version_info.minor) + "-dev")')
echo "installing $PACKAGE_NAME"
apt install $PACKAGE_NAME -y
echo ""

echo "#### Installing python3 libraries setuptools psutil"

python3 -m pip install -U pip
pip3 install setuptools==51.1.1
#python3 -m pip install -U setuptools
pip3 install wheel==0.32.3
pip3 install psutil==5.8.0
echo ""

echo "#### configuring mosquitto server for mqtts"
mkdir -p /etc/mosquitto/ca_certificates
cp -f $dir_path/../certs/mqtt-server.crt /etc/mosquitto/ca_certificates/mqtt-server.crt
cp -f $dir_path/../certs/mqtt-server.key /etc/mosquitto/ca_certificates/mqtt-server.key
cp -f $dir_path/../../certs/mqtt-ca.crt /etc/mosquitto/ca_certificates/mqtt-ca.crt
#creates mosquito confi backup
FILE=/etc/mosquitto/mosquitto.conf.bkp
if [ ! -f "$FILE" ]
  then cp -f /etc/mosquitto/mosquitto.conf /etc/mosquitto/mosquitto.conf.bkp
fi
cp -f $dir_path/mqtt_mosquitto.conf /etc/mosquitto/mosquitto.conf

echo "#### Starting mosquitto Service"
systemctl start mosquitto.service
echo ""

echo "#### Disabling mosquitto Service for not autostart"
systemctl disable mosquitto.service
echo ""
