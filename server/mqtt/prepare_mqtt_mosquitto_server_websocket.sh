#!/bin/bash

echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path

IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi

echo "#### Building mosquitto with websocket"
apt install git libssl-dev libwebsockets-dev libsystemd-dev uuid-dev xsltproc docbook-xsl libcunit1-dev libcjson1 libcjson-dev -y
#apt install libsystemd-dev -y
useradd --system --no-create-home mosquitto

rm -rf ./mosquitto-src
git clone https://github.com/eclipse/mosquitto.git --branch debian-mosquitto mosquitto-src
cd mosquitto-src

#replace WITH_WEBSOCKETS:=no to WITH_WEBSOCKETS:=yes
sed -i 's/WITH_WEBSOCKETS:=no/WITH_WEBSOCKETS:=yes/' config.mk 
sed -i 's/prefix?=\/usr\/local/prefix?=\/usr/' $dir_path/mosquitto-src/config.mk
sed -i 's/WITH_SYSTEMD:=no/WITH_SYSTEMD:=yes/' config.mk 
#remove alread existent mosquitto
make uninstall
rm -rf /etc/mosquitto
rm -f /etc/systemd/system/mosquitto.service
rm -f /etc/systemd/system/multi-user.target.wants/mosquitto.service
systemctl daemon-reload

#setting prefix path


#sed -i 's/\/usr\/sbin\/mosquitto/\/usr\/local\/sbin\/mosquitto/' $dir_path/mosquitto-src/service/systemd/mosquitto.service.notify

#compile
make
#install
make install
cd $dir_path

#Created symlink /etc/systemd/system/multi-user.target.wants/mosquitto.service → /lib/systemd/system/mosquitto.service.
cp -f $dir_path/mosquitto-src/service/systemd/mosquitto.service.notify /lib/systemd/system/mosquitto.service
ln -sf /lib/systemd/system/mosquitto.service /etc/systemd/system/multi-user.target.wants/mosquitto.service
systemctl daemon-reload
echo ""

read -p "Mosquitto installed, Press any key to resume .."

echo "#### Stopping mosquitto Service"
systemctl stop mosquitto.service
echo ""

echo "#### Installing  python3, python3-pip, python3-wheel, gcc, python3-dev mosquitto, libpcap0.8"
apt-get install python3 python3-pip python3-wheel gcc libpcap0.8 -y
PACKAGE_NAME=$(python3 -c 'import sys; print("python" + str(sys.version_info.major) + "." + str(sys.version_info.minor) + "-dev")')
echo "installing $PACKAGE_NAME"
apt install $PACKAGE_NAME -y
echo ""

echo "#### Installing python3 libraries setuptools psutil"

python3 -m pip install -U pip
pip3 install setuptools==51.1.1
#python3 -m pip install -U setuptools
pip3 install wheel==0.32.3
pip3 install psutil==5.8.0
echo ""

echo "#### configuring mosquitto server for mqtts"
mkdir -p /etc/mosquitto/ca_certificates
cp -f $dir_path/../certs/mqtt-server.crt /etc/mosquitto/ca_certificates/mqtt-server.crt
cp -f $dir_path/../certs/mqtt-server.key /etc/mosquitto/ca_certificates/mqtt-server.key
cp -f $dir_path/../../certs/mqtt-ca.crt /etc/mosquitto/ca_certificates/mqtt-ca.crt
#creates mosquito config backup
ORIGINAL_FILE=/etc/mosquitto/mosquitto.conf
FILE=/etc/mosquitto/mosquitto.conf.bkp
if [ -f "$ORIGINAL_FILE" ]
  then
  if [ ! -f "$FILE" ]
      then 
        cp -f /etc/mosquitto/mosquitto.conf /etc/mosquitto/mosquitto.conf.bkp
  fi
fi
cp -f $dir_path/mqtt_mosquitto_websocket.conf /etc/mosquitto/mosquitto.conf

read -p "Mosquitto config files setted, Press any key to resume .."

echo "#### Starting mosquitto Service"
systemctl start mosquitto.service
echo ""

echo "#### Disabling mosquitto Service for not autostart"
systemctl disable mosquitto.service
echo ""

read -p "Mosquitto installed, Press any key to resume .."

echo "#### Removing mosquitto source code"
rm -rf $dir_path/mosquitto-src
echo ""