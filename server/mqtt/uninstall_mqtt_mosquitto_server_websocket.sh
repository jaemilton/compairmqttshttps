#!/bin/bash

echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path

IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi
echo "#### Stopping mosquitto Service"
systemctl stop mosquitto.service
echo ""

echo "#### Removing mosquitto with websocket"
#remove alread existent mosquitto
rm -rf /etc/mosquitto
rm -f /etc/systemd/system/mosquitto.service
rm -f /etc/systemd/system/multi-user.target.wants/mosquitto.service
systemctl daemon-reload

echo "#### Removing user mosquitto"
userdel mosquitto
