#!/bin/bash

echo $0
 
full_path=$(realpath $0)
echo $full_path
 
dir_path=$(dirname $full_path)
echo $dir_path

IDU=$(id -u)
if [ $IDU -ne 0 ] 
then
    echo "Please run as root"
    exit
fi

echo "#### Installing  python3, python3-pip"

apt-get install python3 python3-pip -y
apt-get install python3-wheel -y
PACKAGE_NAME=$(python3 -c 'import sys; print("python" + str(sys.version_info.major) + "." + str(sys.version_info.minor) + "-dev")')
echo "installing $PACKAGE_NAME"
apt install $PACKAGE_NAME -y
echo ""

echo "#### Installing python3 libraries setuptools psutil"
python3 -m pip install -U pip
pip3 install setuptools==51.1.1
#python3 -m pip install -U setuptools
pip3 install wheel==0.32.3
pip3 install psutil==5.8.0
echo ""

echo "#### preparing nethogs lib"
cd $dir_path/../process_monitor_lib
./build_nethogs_lib.sh
echo ""
cd #dir_path

rm -rf /opt/app/process_monitor
mkdir -p /opt/app/process_monitor

echo "#### Copying monotoring app to /opt/app/process_monitor"
cp -f $dir_path/process_monitor_util.py /opt/app/process_monitor/process_monitor_util.py
cp -rf $dir_path/../common_lib /opt/app/process_monitor
cp -rf $dir_path/../process_monitor_lib /opt/app/process_monitor
echo ""

