#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
import sys



# including some directories to PATH 
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'process_monitor_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.path.pardir,
                                'common_lib'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from process_monitor_lib.monitor_type import MonitorType
from common_lib.common_base import CommonBase
from common_lib.common_base import valid_mandatory_parameters
from common_lib.common_base import get_input_parameter_value
from common_lib.common_error import BadUserInputError
from process_monitor_lib.process_monitor import ProcessMonitor


class ProcessMonitorUtil(CommonBase):
    def __init__(self,
                 pid:int,
                 process_name:str,
                 log_directory:str,
                 debug:bool=False,
                 microsecond_update_interval: int = None,
                 monitor_type: MonitorType = MonitorType.BOTH,
                 monitor_just_children:bool=True):
        CommonBase.__init__(self, on_start_handler=self.on_start, on_stop_handler=self.on_stop, debug=debug)
        self.pid = pid
        self.process_name = process_name
        self.log_directory = log_directory
        self.pid_list = []
        self.microsecond_update_interval = microsecond_update_interval
        self.process_monitor = None
        self.monitor_type = monitor_type
        self.monitor_just_children = monitor_just_children
        

    def on_start(self):

        if self.pid:
            self.pid_list.append(self.pid)

        if self.process_name:
            pid_list = ProcessMonitor.get_pid_list_by_name(self.process_name)
            if pid_list:
                self.pid_list.extend(pid_list)
                print(str(len(self.pid_list)) + " process were found for the name: " + self.process_name)

        if len(self.pid_list) > 0:
            print(str(len(self.pid_list)) + " process(es) found to be monitored")
            for pid in self.pid_list:
                print("Process found, pid: " + str(pid)) 

            #Ensure that log directory exists
            if not self.log_directory:
                self.log_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "logs")

            if (not os.path.exists(self.log_directory)):
                os.mkdir(self.log_directory)

            print("Monitoring logs beeing written on directory " + self.log_directory)
            self.process_monitor = ProcessMonitor(process_id_list = self.pid_list, 
                                                    log_directory = self.log_directory, 
                                                    microsecond_update_interval=self.microsecond_update_interval,
                                                    monitor_type=self.monitor_type,
                                                    monitor_just_children=self.monitor_just_children,
                                                    debug=self._debug)

        else:
            if self.pid:
                print("There are no pid " + self.pid + " process running")
            elif self.process_name:
                print("There are no named " + self.process_name + " process running")
            self.stop()

    def on_stop(self):
        if self.process_monitor:
            self.process_monitor.on_stop()
        print("Process monitoring finished")

def start(argv):
    if ('-?' in argv):
        print("""Python process monitor utility to monitor process by name or pid Usage: python3 
                 monitor_monitor_util.py [-n process_name] [-p PID] [-l log_directory] [-u MICROSECONDS_NUMBER] [-v] [-c] [-?]

                -n : name of process to be monitored.
                -p : PID to be monitored.
                -l : Optional, directory where logs will bw written. If nos passed, the logs will be written on current directory inside a folder logs
                -u : Optional, microseconds update interval. When not informed, process statistics will be updated each 1 second
                -t : Optional, type of monitoring, values are CPU or NETWORK or BOTH, if not set, will assume BOTH
                -c : Optional, if set, just children processes will be monitored if the process has children processes
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    #ensure that -n or -p parameters were passed
    elif not(valid_mandatory_parameters(argv, ['-n']) or valid_mandatory_parameters(argv, ['-p'])):
        raise BadUserInputError(
            "Input error. To run, call as python3 monitor_monitor_util.py [-n process_name] [-p PID] [-l log_directory] [-u MICROSECONDS_NUMBER] [-v] [-c] [-?]")
    else:
        process_name = get_input_parameter_value(argv, '-n')
        pid_param = get_input_parameter_value(argv, '-p')
        process_id = None
        microsecond_update_interval = None
        millisecond_update_interval_param = get_input_parameter_value(argv, '-u')
        if millisecond_update_interval_param:
            microsecond_update_interval = int(millisecond_update_interval_param)
        else:
            microsecond_update_interval = 1000000
        
        if pid_param:
            process_id = int(pid_param)

        monitor_type_param = get_input_parameter_value(argv, '-t')
        if not monitor_type_param:
            monitor_type = MonitorType.BOTH
        else:
            monitor_type = MonitorType[monitor_type_param.upper()]

        log_directory = get_input_parameter_value(argv, '-l')
        debug = ('-v' in argv)
        monitor_just_children = ('-c' in argv)

        process_monitor_util = ProcessMonitorUtil(pid=process_id,
                                                process_name=process_name,
                                                log_directory= log_directory,
                                                microsecond_update_interval=microsecond_update_interval,
                                                monitor_type=monitor_type,
                                                monitor_just_children=monitor_just_children,
                                                debug=debug)

        process_monitor_util.start()

if __name__ == '__main__':
    start(sys.argv)