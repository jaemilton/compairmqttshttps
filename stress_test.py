#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from result_consolidation_lib.consolidation_type import ConsolidationType
from typing import List, Tuple

import os
import sys


from stress_test.stress_test_type  import StressTestType

import time
import datetime

from common_lib.common_base import convert_string_to_array, valid_mandatory_parameters
from common_lib.common_base import get_input_parameter_value
from common_lib.common_error import BadUserInputError
from common_lib.common_error import RemoteCommandError
from stress_test.remote_command import RemoteCommand
from stress_test.sub_process_handler import SubProcessHandler
from stress_test.publisher_client import PublisherClient
from stress_test.responder_client import ResponderClient
from common_lib.websocket_client_type import WebsocketClientType


STDOUT_LINE_SEPARATOR = "#########################################"
#CMD="C:\\WINDOWS\\system32\\cmd.exe"
CMD="C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe" if (sys.platform == "win32") else "/bin/bash"
CONSOLIDATION_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "result_consolidation.py")
class StressTest(RemoteCommand):

    REMOTE_SERVICE_COMMAND = "systemctl {status} {service_name}"
    REMOTE_MONITOR_COMMAND = "python3 /opt/app/process_monitor/process_monitor_util.py -n {process_name} -u {update_microsecond_interval} -c"
 
 
    def __init__(self,
                stress_test_type: StressTestType,
                remote_address: str,
                message_count_tuple_array: List[Tuple[int,int]],
                message_text:str,
                interval_seconds: int,
                client_log_directory:str,
                server_log_directory:str,
                websocket_client_type: WebsocketClientType,
                debug: bool) -> None:
        RemoteCommand.__init__(self=self, remote_address=remote_address, debug=debug)
        self.websocket_client_type = websocket_client_type
        self.stress_test_type = stress_test_type
        self.remote_address = remote_address
        self.message_count_tuple_array = message_count_tuple_array
        self.message_text = message_text
        self.interval_seconds = interval_seconds
        self.debug = debug
        self.client_log_directory = client_log_directory
        self.server_log_directory = server_log_directory


    def get_remote_service_name(self) -> str : 
        service_name = None
        if self.stress_test_type == StressTestType.MQTTS:
            service_name = "mosquitto.service"
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS and self.websocket_client_type == WebsocketClientType.SOCKEIO:
            service_name = "flask-websocket-ssl.service"
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS and self.websocket_client_type == WebsocketClientType.WEBSOCKET:
            service_name = "cpp-websocket-ssl.service"
        else:
            raise BadUserInputError("Stress test type not identified " + str(self.stress_test_type))

        return service_name

    def get_remote_process_to_monitor(self) -> str : 
        command_argument = None
        if self.stress_test_type == StressTestType.MQTTS:
            command_argument = "mosquitto"
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS and self.websocket_client_type == WebsocketClientType.SOCKEIO:
            command_argument = "gunicorn"
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS and self.websocket_client_type == WebsocketClientType.WEBSOCKET:
            command_argument = "wss_broadcast_server"
        else:
            raise BadUserInputError("Stress test type not identified " + str(self.stress_test_type))

        return command_argument

    def get_remote_monitor_command(self, update_microsecond_interval):
        return self.REMOTE_MONITOR_COMMAND.format(process_name=self.get_remote_process_to_monitor(), 
                                update_microsecond_interval=update_microsecond_interval)

    def start(self):
        """
        Start the stress test for all stress test message count array 
        """
        
        #step 1 - Ensure that de server side service is started
        self.change_remote_status_service(status="start")
        time.sleep(5)

        responder_client = ResponderClient(stress_test_type=self.stress_test_type, 
                                                remote_address=self.remote_address, debug=self.debug)
        #step 2 - Start de client responder
        responder_client.start_client_responder()
        time.sleep(4)

        for message_count_tuple in self.message_count_tuple_array:
            message_count, update_microsecond_interval = message_count_tuple
            #step 3 - Ensure that de server side monitoring is started to generate the performance logs
            self.start_remote_monitoring(update_microsecond_interval=update_microsecond_interval)

            publisher_client = PublisherClient(stress_test_type=self.stress_test_type, 
                                                remote_address=self.remote_address,
                                                message_count=int(message_count),
                                                message_text=self.message_text,
                                                log_directory=self.client_log_directory,
                                                debug=self.debug)
            #step 4 - Start de client publisher to send the test messages
            publisher_client.start_client_publisher()

            if self.interval_seconds > 0.0:
            #step 5 - Wait some seconds
                print(">>>>> Wainting {second_interval} seconds"
                            .format(second_interval=self.interval_seconds))
                time.sleep(self.interval_seconds)

            #step 6 - Stops server side monitoring
            self.stop_remote_monitoring()

        #step 7 - Stops client responder
        responder_client.stop_subprocess()

        #step 8 - Stops server side service
        self.change_remote_status_service(status="stop")

        #step 9 - Copy the server side log files ro server_log_directory
        self.copy_server_logs()

        #step 10 - Remove the server side log files
        self.remove_remote_logs()



    def change_remote_status_service(self, status):

        print(">>>>> Connecting to server to run {status} service {service_name}".format(status=status, 
                                                                        service_name=self.get_remote_service_name()))

        self.start_service_result = self.execute_remote_command(
                                    command=self.REMOTE_SERVICE_COMMAND.format(status=status,
                                                    service_name=self.get_remote_service_name()))

        if self.start_service_result.failed:
            self.print_stderr(result=self.start_service_result)
            raise RemoteCommandError("Error to start the service " + self.get_remote_service_name())

    def start_remote_monitoring(self, update_microsecond_interval):
        print(">>>>> monitoring of " + self.get_remote_process_to_monitor() + " started.")
        self.remote_monitot_result = self.execute_remote_command(command=self.get_remote_monitor_command(update_microsecond_interval), 
                                                asynchronous=True, 
                                                pty=True)
        time.sleep(2)
        print(STDOUT_LINE_SEPARATOR)
        self.print_stdout(result=self.remote_monitot_result)
        print(STDOUT_LINE_SEPARATOR)

    def stop_remote_monitoring(self):
        self.remote.send_interrupt(BadUserInputError("Error to interrupt remote execution"))
        time.sleep(1)
        self.ssh_connection.close()
        print(">>>>> monitoring of " + self.get_remote_process_to_monitor() + " stopped.")

    def copy_server_logs(self):
        print(">>>>> copying logs from server side.")
        SCP_COMMAND = "scp root@{server_name}:/opt/app/process_monitor/logs/* {server_log_directory}"
        scp_sub_process = SubProcessHandler(debug=self.debug)
        print(STDOUT_LINE_SEPARATOR)
        sub_process_command=SCP_COMMAND.format(server_name=self.remote_address,
                                                server_log_directory=self.server_log_directory)
        scp_sub_process.start_subprocess(sub_process_command=sub_process_command, executable=CMD, shell=True, wait_to_finish=True)
        print(STDOUT_LINE_SEPARATOR)

    def remove_remote_logs(self):
        REMOVE_SERVER_LOGS_COMMAND = "rm -f /opt/app/process_monitor/logs/*"
        print(">>>>> Connecting to server remove server logs")
        self.remove_log_server_result = self.execute_remote_command(command=REMOVE_SERVER_LOGS_COMMAND)
        
        print(STDOUT_LINE_SEPARATOR)
        if self.remove_log_server_result.failed:
            self.print_stderr(result=self.remove_log_server_result)
            raise RemoteCommandError("Error to remove server logs")
        else:
            self.print_stdout(result=self.remove_log_server_result)
        print(STDOUT_LINE_SEPARATOR)


############################ STATIC METHODS ############################################


def get_type_log_directory(stress_test_type: StressTestType):
    type_log_directory = None
    if stress_test_type == StressTestType.MQTTS:
        type_log_directory = "mqtts"
    elif stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS:
        type_log_directory = "https_websocket"
    return type_log_directory



def get_new_logs_directory() -> str:
    """
    Returns the base_logs_directory:str
    """
    new_logs_directory = None
    base_log_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "TestResults")
    log_directory_number = 0
    datestr = datetime.datetime.now().strftime("%Y%m%d")
    while not new_logs_directory:
        log_directory_number +=1
        log_directory_name = "Test_{datestr}_{log_directory_number:0>2}".format(datestr=datestr, 
                                                                                log_directory_number=log_directory_number)
        log_directory = os.path.join(base_log_directory,log_directory_name)
        if not os.path.exists(log_directory):
            new_logs_directory = log_directory

    return new_logs_directory


def get_logs_directories(new_logs_directory:str, stress_test_type: StressTestType):
    """
    Returns a tuple (client_logs_directory:str, server_logs_directory:str) 
    """
    client_log_directory = None
    server_log_directory = None
    
    client_log_directory = os.path.join(new_logs_directory, 
                                        get_type_log_directory(stress_test_type), 
                                        "client_side")
        
    server_log_directory = os.path.join(new_logs_directory, 
                                    get_type_log_directory(stress_test_type), 
                                    "server_side")

    return (client_log_directory, server_log_directory)

def create_log_dir(log_directory):
    if not os.path.exists(log_directory):
        os.makedirs(log_directory)


def start_consolidation(consolidation_type:ConsolidationType, consolidation_column:str, new_logs_directory:str, debug:bool = False):
        print(">>>>> Creating Consolidation for " + consolidation_type.name + " side, grouping by " + consolidation_column)
        command = "{result_consolidation_path} -d {new_logs_directory} -t {consolidation_type} -c \"{consolidation_column}\""

        sub_process_handler = SubProcessHandler(debug=debug)

        sub_process_command = convert_string_to_array(command.format(result_consolidation_path=CONSOLIDATION_PATH,  
                                                                        new_logs_directory=new_logs_directory,
                                                                        consolidation_type=consolidation_type.name,
                                                                        consolidation_column=consolidation_column
                                                                    )
                                                    )
                                                        

        sub_process_handler.start_python_script_subprocess(sub_process_command=sub_process_command, 
                                                            wait_to_finish=True)


def start_stress_test(stress_test_type: StressTestType,
                remote_address: str,
                message_count_tuple_array: List[Tuple[int,int]],
                message_text:str,
                websocket_client_type: WebsocketClientType,
                interval_seconds: int = 10,
                debug: bool = False):

    stress_test_type_list = []
    if stress_test_type == StressTestType.ALL:
        stress_test_type_list.append(StressTestType.MQTTS)
        stress_test_type_list.append(StressTestType.WEBSOCKET_OVER_HTTPS)
    else:
        stress_test_type_list.append(stress_test_type)

    new_logs_directory = get_new_logs_directory()
    #ensure that logs directories exists
    create_log_dir(new_logs_directory)


    for current_stress_test_type in stress_test_type_list:
        (client_log_directory, server_log_directory) = get_logs_directories(new_logs_directory=new_logs_directory,
                                                                     stress_test_type= current_stress_test_type)

        #ensure that logs directories exists
        create_log_dir(client_log_directory)
        create_log_dir(server_log_directory)

        stress_test = StressTest(stress_test_type=current_stress_test_type,
                                        remote_address=remote_address,
                                        message_count_tuple_array=message_count_tuple_array,
                                        message_text=message_text,
                                        interval_seconds=interval_seconds,
                                        websocket_client_type=websocket_client_type,
                                        client_log_directory=client_log_directory,
                                        server_log_directory=server_log_directory,
                                        debug=debug)

        stress_test.start()

    #step 11 - Generate consolidated server and client logs
    start_consolidation(consolidation_type=ConsolidationType.SERVER, 
                        new_logs_directory=new_logs_directory, 
                        consolidation_column="total_time_microseconds", 
                        debug=debug)
                        
    start_consolidation(consolidation_type=ConsolidationType.CLIENT, 
                        new_logs_directory=new_logs_directory, 
                        consolidation_column="total_microseconds", 
                        debug=debug)

    start_consolidation(consolidation_type=ConsolidationType.CLIENT, 
                        new_logs_directory=new_logs_directory, 
                        consolidation_column="accumulated_delta_microseconds", 
                        debug=debug)


def get_count_update_interval_tuple(message_count_param: str) -> List[Tuple[int, int]]:
    message_count_tuple_array = []
    message_count_array = message_count_param.split(",")
    for message_count_param_value in message_count_array:
        param_value = message_count_param_value.replace("[","").replace("]","").split("|")
        message_count = int(param_value[0])
        update_microsecond_interval = None
        if len(param_value) == 2:
            update_microsecond_interval = int(param_value[1])
        message_count_tuple_array.append((message_count, update_microsecond_interval))
    return message_count_tuple_array

def start(argv):
    if '-?' in argv:
        print("""Application to run the stress test od mqtts or websocket over https
        Usage: python3 stress_test.py [-t TYPE_STRESS_TEST] -r server_name -c [message_count|monitor_update_interval] 
        -m MESSAGE_TEXT -i INTERVAL_IN_SECONDS [-w websocket_client_connection_type] [-?] [-v] 
                
                -w : websocket client connection type, values are SOCKEIO or WEBSOCKET, if not set, will assume WEBSOCKET
                -t : Optional, type of stress test, values are MQTTS or WEBSOCKET_OVER_HTTPS, if not set, will assume ALL
                -r : remote server address, FQDN name or IP
                -c : array tuple [message_count|monitor_update_interval] separetad by comma for stress test
                    Example: -c "[10|200], [100|200], [500|300]", it must be between quotation marks
                -m: message text to be used on stress test
                -i: interval in second between stress tests executions
                -? : display this help.
                -v : verbose mode - enable all logging types.
                """)
    elif valid_mandatory_parameters(argv, ['-r', '-c', '-m']):
        raise BadUserInputError(
            "Input error. To run, call as python3 stress_test.py [-t TYPE_STRESS_TEST] -r server_name -c [message_count|monitor_update_interval] "
            "-m MESSAGE_TEXT -i INTERVAL_IN_SECONDS [-w websocket_client_connection_type] [-?] [-v]")
    else:
        websocket_client_type_param = get_input_parameter_value(argv, '-w')
        if not websocket_client_type_param:
            websocket_client_type = WebsocketClientType.WEBSOCKET
        else:
            websocket_client_type = WebsocketClientType[websocket_client_type_param.upper()]

        stress_test_type_param = get_input_parameter_value(argv, '-t')
        if not stress_test_type_param:
            stress_test_type = StressTestType.ALL
        else:
            stress_test_type = StressTestType[stress_test_type_param.upper()]

        remote_address = get_input_parameter_value(argv, '-r')
        message_count_param = get_input_parameter_value(argv, '-c') 
        
        message_count_tuple_array = get_count_update_interval_tuple(message_count_param)

        message_text = get_input_parameter_value(argv, '-m')
        interval_seconds_param = get_input_parameter_value(argv, '-i')
        interval_seconds = 10 #default interval
        if interval_seconds_param:
            interval_seconds = int(interval_seconds_param)
        
        debug = ('-v' in argv)

        start_stress_test(stress_test_type=stress_test_type,
                remote_address=remote_address,
                message_count_tuple_array=message_count_tuple_array,
                message_text=message_text,
                interval_seconds=interval_seconds,
                websocket_client_type=websocket_client_type,
                debug=debug)


if __name__ == '__main__':
    start(sys.argv)
