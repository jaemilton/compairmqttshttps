#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
from stress_test.sub_process_handler import SubProcessHandler
from stress_test.stress_test_type import StressTestType
from common_lib.common_base import convert_string_to_array


class PublisherClient(SubProcessHandler):
    MQTT_CLIENT_PUBLISHER_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir, "client", "mqtt_client_publisher.py")
    WSS_CLIENT_PUBLISHER_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir, "client", "https_websocket_client_publisher.py")
    MQTTS_PUBLISHER_COMMAND =  "\"{client_publisher_path}\" -n client01 -h \"{server_name}\" -p 8883 -t TOPIC_01 -s TOPIC_01_R -c {message_count} -m \"{message_text}\" -l \"{log_directory}\""
    WSS_PUBLISHER_COMMAND = "\"{client_publisher_path}\" -n client01 -a \"wss://{server_name}\" -e TOPIC_01 -s TOPIC_01_R -c {message_count} -m \"{message_text}\" -l \"{log_directory}\""

    def __init__(self,
                stress_test_type: StressTestType,
                remote_address: str,
                message_count: int,
                message_text:str,
                log_directory:str,
                debug: bool=False) -> None:
        self.stress_test_type = stress_test_type
        self.remote_address = remote_address
        self.message_count = message_count
        self.message_text = message_text
        self.log_directory = log_directory
        self.debug = debug
        SubProcessHandler.__init__(self=self, debug=debug)

    def _get_publisher_command(self):
        stress_test_command = None
        client_publisher_path = None
        if self.stress_test_type == StressTestType.MQTTS:
            stress_test_command = self.MQTTS_PUBLISHER_COMMAND
            client_publisher_path = self.MQTT_CLIENT_PUBLISHER_PATH
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS:
            stress_test_command = self.WSS_PUBLISHER_COMMAND
            client_publisher_path = self.WSS_CLIENT_PUBLISHER_PATH

        return convert_string_to_array(stress_test_command.format(client_publisher_path=client_publisher_path,
                                        server_name=self.remote_address,
                                        message_count=self.message_count, 
                                        message_text=self.message_text,
                                        log_directory=self.log_directory))


  

    def start_client_publisher(self):
        #start the stress test
        print(">>>>> Starting stress test {test_type} client sending {message_count} messages"
                    .format(test_type=self.stress_test_type, message_count=self.message_count))
        self.start_python_script_subprocess(sub_process_command=self._get_publisher_command(), 
                                wait_to_finish=True)

    