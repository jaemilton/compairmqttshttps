#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import fabric
from fabric.runners import Result

class RemoteCommand(object):
    def __init__(self,
                remote_address: str,
                debug: bool=False) -> None:
        self.remote_address = remote_address
        self.ssh_connection = fabric.Connection(host=self.remote_address, user="root")
        self.remote = fabric.Remote(self.ssh_connection)
        
        
        self.debug = debug

    def execute_remote_command(self, 
                                command:str,
                                asynchronous=False, 
                                pty=False) -> Result: 
        if self.debug:
            print(">>>>> Executing remote command => " + command)
        return self.remote.run(command=command, asynchronous=asynchronous, pty=pty)

    def print_stdout(self, result: Result):
        if hasattr(result, 'runner'):
            for line in result.runner.stdout:
                print(line,  end = "\r") #print the remote monitoring stdout
        else:
            print(result.stdout)

    def print_stderr(self, result: Result):
        if result.failed:
            if hasattr(result, 'runner'):
                for line in result.runner.stderr:
                    print(line,  end = "\r") #print the remote monitoring error
            else:
                print(result.stderr)
