#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os
from stress_test.sub_process_handler import SubProcessHandler
from stress_test.stress_test_type import StressTestType
from common_lib.common_base import convert_string_to_array


class ResponderClient(SubProcessHandler):

    MQTTS_EXPECTED_STDOUT_LINE = "Subscribed: rc: 1 - topic: TOPIC_01"
    WSS_EXPECTED_STDOUT_LINE = "Client connected. Listing to events TOPIC_01"
    MQTT_CLIENT_RESPONDER_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir, "client", "mqtt_client_responder.py")
    WSS_CLIENT_RESPONDER_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir, "client", "https_websocket_client_responder.py")
    MQTTS_RESPONDER_COMMAND =  "\"{client_responder_path}\" -n responder01 -h {server_name} -p 8883 -r TOPIC_01_R -s TOPIC_01"
    WSS_RESPONDER_COMMAND = "\"{client_responder_path}\" -n responder01 -a wss://{server_name} -e TOPIC_01_R -s TOPIC_01"

    def __init__(self,
                stress_test_type: StressTestType,
                remote_address: str,
                debug: bool=False) -> None:
        self.stress_test_type = stress_test_type
        self.remote_address = remote_address
        self.debug = debug
        SubProcessHandler.__init__(self=self, debug=debug)


    def _get_responder_command(self) :
        stress_test_command = None
        client_responder_path = None
        if self.stress_test_type == StressTestType.MQTTS:
            stress_test_command = self.MQTTS_RESPONDER_COMMAND
            client_responder_path = self.MQTT_CLIENT_RESPONDER_PATH
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS:
            stress_test_command = self.WSS_RESPONDER_COMMAND
            client_responder_path = self.WSS_CLIENT_RESPONDER_PATH

        return convert_string_to_array(stress_test_command.format(client_responder_path=client_responder_path,
                                        server_name=self.remote_address))

    def _get_expected_stdout_line(self):
        expected_stdout_line = None

        if self.stress_test_type == StressTestType.MQTTS:
            expected_stdout_line = self.MQTTS_EXPECTED_STDOUT_LINE
        elif self.stress_test_type == StressTestType.WEBSOCKET_OVER_HTTPS:
            expected_stdout_line = self.WSS_EXPECTED_STDOUT_LINE
        
        return expected_stdout_line

    def start_client_responder(self):
        print(">>>>> Starting stress test {test_type} client responder"
                    .format(test_type=self.stress_test_type))
        self.start_python_script_subprocess(sub_process_command=self._get_responder_command(), 
                                wait_to_finish=False)
        #print(STDOUT_LINE_SEPARATOR)
        #self.print_stdout(wait_to_output_line=self._get_expected_stdout_line())
        #print(STDOUT_LINE_SEPARATOR)

