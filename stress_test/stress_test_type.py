#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from enum import Enum

class StressTestType(Enum):

     MQTTS = 1
     WEBSOCKET_OVER_HTTPS = 2
     ALL = 3