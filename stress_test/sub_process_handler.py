#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys
import subprocess
from typing import List
from common_lib.common_error import BadUserInputError

class SubProcessHandler(object):
    def __init__(self,
                debug: bool = False) -> None:
        self.sub_process = None
        self.debug = debug

    def start_subprocess(self, sub_process_command:str, executable=None, shell=False, stdout=None, stderr=None, wait_to_finish:bool=False):
        """
        Start the child subprocess of client responder
        """
        if not sub_process_command:
            raise BadUserInputError("the sub_process_command must be defined")

        #"C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
        print(">>>>> Executing local command => " + " ".join(sub_process_command))
        if self.debug:
            print(">>>>> Execution Options => shell=" + str(shell) + " stdout="+ str(stdout) + " stderr=" + str(stderr) )
        self.sub_process = subprocess.Popen(sub_process_command, executable=executable,  shell=shell, stdout=stdout, stderr=stderr)
        if wait_to_finish:
            self.sub_process.wait()
        
        subprocess.run

    def start_python_script_subprocess(self, sub_process_command:List[str], stdout=None, stderr=None, wait_to_finish:bool=False):
        """
        Start the child subprocess of client responder
        """
        if not sub_process_command:
            raise BadUserInputError("the sub_process_command must be defined")

        #"C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
        if self.debug:
            print(">>>>> Executing local command => " + " ".join(sub_process_command))
            print(">>>>> Execution Options => stdout="+ str(stdout) + " stderr=" + str(stderr) )
        
        sub_process_command.insert(0, sys.executable)
        self.sub_process = subprocess.Popen(sub_process_command, stdout=stdout, stderr=stderr)
        if wait_to_finish:
            self.sub_process.wait()
    

    def stop_subprocess(self):
        """
        Stop the child subprocess of client responder
        """
        if self.sub_process:
            self.sub_process.kill()

    def print_stdout(self, wait_to_output_line:str = None):
        print("wainting for \"" + wait_to_output_line + "\" to be printend to continue")
        print_stdout = True
        while print_stdout:
            line = self.sub_process.stdout.readline().decode('utf-8')
            if not line or not wait_to_output_line or (line.lower().startswith(wait_to_output_line.lower())):
                print_stdout = False
            print(line, end="\r")


    