from common_lib.common_time_control import get_microseconds_between_timestamps
from datetime import datetime
from datetime import timedelta
import time



ts1 = datetime.timestamp(datetime.now())
time.sleep((1.0 / 1000000) * 1)
ts2 = datetime.timestamp(datetime.now())
us_t = get_microseconds_between_timestamps(start_timestamp=ts1, end_timestamp=ts2)



timestamp_list = [1610849334.74605,
1610849334.74605,
1610849337.00180,
1610849337.0018,
1610849338.00784,
1610849339.38305,
1610849341.03428,
1610849343.1729,
1610849343.1729,
1610849343.1729,
1610849344.45543]

last_timestamp = None
for timestamp in timestamp_list:
#for i in range(100):

    if not last_timestamp:
        last_timestamp = timestamp
    # current date and time
    #now = datetime.now()

    #timestamp = datetime.timestamp(now)

    print("timestamp=" + str(timestamp))
    #print("formatted timestamp=%.6f"%(timestamp))
    print("formatted timestamp=", "{timestamp:.6f}".format(timestamp=timestamp))

    timedelta(0, 7, 105000)
    

    last_datetime = datetime.fromtimestamp(last_timestamp)
    current_datetime = datetime.fromtimestamp(timestamp)
    print("Time in microseconds = " + str((current_datetime - last_datetime).microseconds))
    last_datetime = current_datetime

    print("current_datetime =", current_datetime)
    print("type(current_datetime) =", type(current_datetime))

    time.sleep(0.1)