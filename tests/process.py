#!/usr/bin/env python2.7


import os.path
import time
import psutil
from process_monitor_lib.cpu_info import CpuInfo
p = psutil.Process()
pn = CpuInfo()


#!/usr/bin/env python2.7


def get_cpu_times(pid):
    # Read first line from /proc/stat. It should start with "cpu"
    # and contains times spend in various modes by all CPU's totalled.
    #

    with open("/proc/" + str(pid) + "/stat") as procfile:
        cpustats = procfile.readline().split()

    # Sanity check
    #
    if cpustats[0] != str(pid):
        raise ValueError("First line of /proc/stat not recognised")

    # Refer to "man 5 proc" (search for /proc/stat) for information
    # about which field means what.
    #
    # Here we do calculation as simple as possible:
    #
    # CPU% = 100 * time-doing-things / (time_doing_things + time_doing_nothing)
    #

    user_time = int(cpustats[13])    # time spent in user space
    nice_time = int(cpustats[15])    # 'nice' time spent in user space
    system_time = int(cpustats[14])  # time spent in kernel space

    idle_time = int(cpustats[4])    # time spent idly
    iowait_time = int(cpustats[5])    # time spent waiting is also doing nothing

    time_doing_things = user_time + nice_time + system_time
    time_doing_nothing = idle_time + iowait_time

    return time_doing_things, time_doing_nothing


def cpu_percentage(pid):
     prev_time_doing_things = 0
     prev_time_doing_nothing = 0
     time_doing_things, time_doing_nothing = get_cpu_times(pid)
     diff_time_doing_things = time_doing_things - prev_time_doing_things
     diff_time_doing_nothing = time_doing_nothing - prev_time_doing_nothing
     cpu_percentage = 100.0 * diff_time_doing_things/ (diff_time_doing_things + diff_time_doing_nothing)

     # remember current values to subtract next iteration of the loop
     #
     prev_time_doing_things = time_doing_things
     prev_time_doing_nothing = time_doing_nothing

     # Output latest perccentage
     #
     return cpu_percentage


while True:
     print(pn.cpu_percent())
     print(p.cpu_percent())
     
