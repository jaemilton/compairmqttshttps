#!/usr/bin/env python

# WSS (WS over TLS) client example, with a self-signed certificate

import os
import ssl
import websocket
import threading
import time


#ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
ca_cert_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        'certs',
        'mqtt-ca.crt')
#mqtt_server_ca = pathlib.Path(__file__).parent.with_name("certs/mqtt-ca.crt")
#ssl_context.load_verify_locations(ca_cert_path)

#os.environ['REQUESTS_CA_BUNDLE'] = ca_cert_path
os.environ['WEBSOCKET_CLIENT_CA_BUNDLE'] = ca_cert_path


def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def run(*args):
    ws = args[0]
    for i in range(10000):
        #time.sleep(1)
        ws.send("Hello %d" % i)
    time.sleep(1)
    ws.close()
    print("thread terminating...")

def on_open(ws):
    mqtt_loop_thead = threading.Thread(target=run, args=[ws])
    mqtt_loop_thead.daemon = True
    mqtt_loop_thead.start()

if __name__ == "__main__":
    #websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://mqtt-server/topic_01",
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)

    #ws.run_forever(sslopt={'ca_cert_path': ca_cert_path})
    ws.run_forever()