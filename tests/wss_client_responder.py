#!/usr/bin/env python

# WSS (WS over TLS) client example, with a self-signed certificate

import os
import sys
import websocket
import threading
import time
from signal import signal, SIGINT, SIGTERM

class WssClientResponder(object):

   


    #mqtt_server_ca = pathlib.Path(__file__).parent.with_name("certs/mqtt-ca.crt")
    #ssl_context.load_verify_locations(ca_cert_path)

    #os.environ['REQUESTS_CA_BUNDLE'] = ca_cert_path
    


    def __init__(self,
                    ca_cert_path,
                    certfile_path,
                    keyfile_path) -> None:
            # Tell Python to run the handler() function when SIGINT is recieved
        signal(SIGINT, self.exit_handler)
        signal(SIGTERM, self.exit_handler)
        self.running = False
        self.message_counter = 0

        #websocket.enableTrace(True)
        ws = websocket.WebSocketApp("wss://mqtt-server/topic_01",
                              on_open = self.on_open,
                              on_message = self.on_message,
                              on_error = self.on_error,
                              on_close = self.on_close)


        ws.run_forever(sslopt={ "ca_certs": ca_cert_path, "keyfile": keyfile_path, "certfile": certfile_path})


    def run(self, *args):
        ws = args[0]
        running = True
        while running:
            time.sleep(1)
        ws.close()
        print("thread terminating...")

    def on_message(self, ws, message):
        self.message_counter += 1
        print("receives " + str(self.message_counter) + " messages",  end = "\r")
        #print("### closed ###")

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        print("### closed ###")


    def exit_handler(self, signal_received, frame):
        running = False
        print("Exiting...")
        sys.exit(0) 

    def on_open(self, ws):
        print("Connected waiting for messages...")
        mqtt_loop_thead = threading.Thread(target=self.run, args=[ws])
        mqtt_loop_thead.daemon = True
        mqtt_loop_thead.start()

if __name__ == "__main__":
     #ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    ca_cert_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            os.path.pardir,
            'certs',
            'mqtt-ca.crt')

    certfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        "client",
        "configs",
        "certs",
        "mqtt-publisher.crt")

    keyfile_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        "client",
        "configs",
        "certs",
        "mqtt-publisher.key")

    #os.environ['WEBSOCKET_CLIENT_CA_BUNDLE'] = ca_cert_path
    wss_responder = WssClientResponder(ca_cert_path=ca_cert_path, certfile_path=certfile_path, keyfile_path=keyfile_path)
    
